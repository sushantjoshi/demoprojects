﻿using OpenGL;
using System.Collections.Generic;
using Tao.FreeGlut;


namespace DemoProgram1 {
  public class KTree<T> {
    public KTree() {

    }
    public KTree(T nodeData) {
      _nodeData = nodeData;
    }

    public KTree<T> AddChild(T nodeData) {
      KTree<T> newNode = new KTree<T>(nodeData);
      _children.Add(newNode);
      return newNode;
    }

    public void AddToDisplayList(T nodeData) {
      _displayList.Add(nodeData);
    }


    public void DepthTraversal(bool renderingEntryExit, bool renderingVolume) {
      //do something with the _nodeData
      IGraphicObject graphicObject = _nodeData as IGraphicObject;
      if (graphicObject != null) {
        graphicObject.Animate(RenderState.Default);

        SceneGraph<IGraphicObject>.transformStack.Push(graphicObject.Transform);

        Gl.DepthMask(true);
        SceneGraph<IGraphicObject> sgInstance = this as SceneGraph<IGraphicObject>;
        if(sgInstance != null)
          graphicObject.Render(RenderState.Default, sgInstance);

        //now traverse each child subtree
        foreach (KTree<T> subTree in _children) {
          subTree.DepthTraversal(renderingEntryExit, renderingVolume);
        }

        SceneGraph<IGraphicObject>.transformStack.Pop();
      }
    }

    protected T _nodeData;
    protected List<KTree<T>> _children = new List<KTree<T>>();
    protected List<T> _displayList = new List<T>();
  }

  public enum RenderState {
    Shadow,
    Reflection,
    Bloom,
    Default
  }

  public class SceneGraph<T> : KTree<T> {
    public static Matrix4 ProjectionMatrix {
      get;
      private set;
    }

    public static Matrix4 ReflectionProjectionMatrix {
      get;
      private set;
    }

    public static Matrix4 ViewMatrix {
      get {
        Program demoProgram = Program.Instance;
        return Matrix4.LookAt(demoProgram.cameraLocation, Vector3.Zero, demoProgram.cameraYAxis);
      }
      set {

      }
    }

    public static Matrix4 ShadowMapCamera
    {
      get;
      set;
    }

    public static Matrix4 ReflectionMapCamera {
      get;
      set;
    }

    public static Matrix4 BiasMatrix {
      get;
      private set;
    }

    private RenderBloom _renderBloom;
    private MergeTextures _mergeTextures;

    public SceneGraph()
      : base() {
      transformStack.Push(Matrix4.Identity);
      //ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(1.1f, (float)_width / _height, 0.0001f, 80.0f);
      ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(1.1f, (float)_width / _height, 10.0f, 55.0f);
      //ProjectionMatrix = Matrix4.CreateOrthographic(50.0f, 40.0f, -40.0f, 40.0f);
      ReflectionProjectionMatrix = Matrix4.CreateOrthographic(1.2f, 0.4f, -2.05f, 10.0f);

      float[] biasMatrixParams = new float[] { 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f };
      BiasMatrix = new Matrix4(biasMatrixParams);//.Transpose();
                                                 //BiasMatrix = Matrix4.Identity;

      _renderBloom = new RenderBloom();
      _renderBloom.InitialiseData();

      _mergeTextures = new MergeTextures();
      _mergeTextures.InitialiseData();
    }

    private void CreateShadowMap(RenderState renderState)
    {
      SetUpCameraForShadowMap();
      Gl.Viewport(0, 0, 1000, 800);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._shadowFrameBufferName[0]);
      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
      RenderDisplayList(renderState);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
    }

    private void CreateBloomTexture(RenderState renderState)
    {
      Gl.Viewport(0, 0, 1000, 800);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._bloomFrameBufferName[0]);

      //Gl.ClearColor(0.0f, 0.3f, 0.0f, 0.0f);
      //Gl.ClearColor(1.0f,0.0f,0.0f,0.0f);
      //Gl.Enable(EnableCap.Blend);
      //Gl.BlendFunc(BlendingFactorSrc.SrcAlpha,BlendingFactorDest.OneMinusSrcAlpha);
      //Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
      RenderDisplayList(RenderState.Bloom);
      _renderBloom.Render(0, null);
      Gl.Disable(EnableCap.Blend);

      //Gl.ClearColor(0.5f, 0.5f, 0.5f, 0.0f);
    }

    private void CreateReflectionMap(RenderState renderState) {
      Gl.Viewport(0, 0, 1000, 800);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._reflectionFrameBufferName[0]);
      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit|ClearBufferMask.StencilBufferBit);
      RenderDisplayList(RenderState.Reflection);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
    }

    private int _width = 1000;
    private int _height = 800;
    private void AnimateObjects(RenderState renderStateIn) {
      foreach (var element in _displayList) {
        IGraphicObject displayElement = element as IGraphicObject;
        if (displayElement != null) {
          displayElement.Animate(renderStateIn);
        }
      }
    }

    public List<T> DisplayList {
      get {
        return _displayList;
      }
    }

    private int counter = 0;
    private static bool _debug = false;
    public void RenderComplete()
    {
      AnimateObjects(RenderState.Reflection);
      CreateReflectionMap(RenderState.Reflection);

      AnimateObjects(RenderState.Bloom);
      CreateBloomTexture(RenderState.Bloom);

      AnimateObjects(RenderState.Shadow);
      CreateShadowMap(RenderState.Shadow);

      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._dummyFrameBufferName[0]);
      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

      AnimateObjects(RenderState.Default);
      RenderDisplayList(RenderState.Default);

      //_renderBloom.Render(Program.Instance._bloomTexture[0], null);

      Gl.BindFramebuffer(FramebufferTarget.Framebuffer,0);
      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
      _mergeTextures.Merge(Program.Instance._dummyFrameBufferTexture[0], Program.Instance._bloomTexture[0]);
    }
    private void RenderDisplayList(RenderState renderState) {
      foreach (var element in _displayList) {
        IGraphicObject displayElement = element as IGraphicObject;
        if (displayElement != null) {
          SceneGraph<IGraphicObject>.transformStack.Push(displayElement.Transform);
          Gl.DepthMask(true);
          SceneGraph<IGraphicObject> sgInstance = this as SceneGraph<IGraphicObject>;
          displayElement.Render(renderState, sgInstance);
          SceneGraph<IGraphicObject>.transformStack.Pop();
        }
      }
    }


    private void SetUpCameraForShadowMap() {
      foreach (var element in _displayList) {
        IGraphicObject displayElement = element as IGraphicObject;
        SphereLight lightSource = displayElement as SphereLight;
        if (lightSource != null) {
          //first of all find the location of light in global cood system.
          Vector4 lightSourceOrigin = new Vector4(lightSource.Origin(), 1);
          Vector4 lightSourceLocation = lightSource.Transform.Transpose() * lightSourceOrigin;

          Vector3 cameraLocation = new Vector3(lightSourceLocation.X, lightSourceLocation.Y, lightSourceLocation.Z);
          ShadowMapCamera = Matrix4.LookAt(
            cameraLocation, 
            new Vector3(0.0f, 0.0f, 0.0f), 
            Vector3.UnitY);
          //Program.Instance.cameraLocation = cameraLocation;
          //Program.Instance.cameraYAxis = Vector3.UnitY;

        }
      }
    }

    public SceneGraph(T nodeData)
            : base(nodeData) {
      transformStack.Push(Matrix4.Identity);
      //ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(0.55f, (float)1000 / 750, 0.01f, 30f);
      ProjectionMatrix = Matrix4.CreateOrthographic(3.6f, 2.5f, -1.8f, 2.8f);
      ReflectionProjectionMatrix = Matrix4.CreateOrthographic(1.2f, 0.4f, -2.05f, 10.0f);

      float[] biasMatrixParams = new float[] { 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f };
      BiasMatrix = new Matrix4(biasMatrixParams);//.Transpose();
                                                 //BiasMatrix = Matrix4.Identity;
    }

    public static Stack<Matrix4> transformStack = new Stack<Matrix4>();
  }
}