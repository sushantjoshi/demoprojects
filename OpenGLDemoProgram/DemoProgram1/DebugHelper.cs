﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.IO;

namespace DemoProgram1 {
  class RenderTextureToScreenDebugObject {
    private VBO<Vector3> meshData;
    private VBO<Vector3> colorData;
    private VBO<Vector2> uvParamData;
    private VBO<int> meshIndices;
    private ShaderProgram _shader;
    private static bool _updateShader = true;

    public void InitialiseData() {
      meshData = new VBO<Vector3>(new Vector3[] { new Vector3(-1, -1, 0), new Vector3(-1, 1, 0), new Vector3(1, 1, 0), new Vector3(1, -1, 0), });
      colorData = new VBO<Vector3>(new Vector3[] { new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1) });
      uvParamData = new VBO<Vector2>(new Vector2[] { new Vector2(0.0f, 0.0f), new Vector2(0.0f, 1.0f), new Vector2(1.0f, 1.0f), new Vector2(1.0f, 0.0f) });

      meshIndices = new VBO<int>(new int[] { 0, 1, 2, 2, 3, 0 }, BufferTarget.ElementArrayBuffer);
      ReadVertexShaderFromFile();
      ReadFragmentShaderFromFile();

      CreateShader();
      CreateTexture();
    }

    private static Texture _textureToDebug;
    public void CreateTexture() {
      // load a debug texture
      if (_textureToDebug == null) {
        _textureToDebug = new Texture("..\\..\\Shaders\\chessboard.png");
      }
    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public void Render(uint textureToDebug, SceneGraph<IGraphicObject> sgInstance) {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }


      Gl.UseProgram(_shader);
      //Gl.BindTexture(_textureToDebug);

      Gl.BindBufferToShaderAttribute(meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(uvParamData, _shader, "vertexUV");
      Gl.BindBuffer(meshIndices);

      Program demoprogram = Program.Instance;
      if (demoprogram != null)
      {
        Vector3 newLocation = new Vector3(0, 0, 1);
        _shader["shadow_camera_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
        _shader["view_matrix"].SetValue(Matrix4.LookAt(newLocation, Vector3.Zero, Vector3.UnitY));
        _shader["biasMatrix"].SetValue(SceneGraph<IGraphicObject>.BiasMatrix);
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
        _shader["model_matrix"].SetValue(Matrix4.Identity);

        Gl.ActiveTexture(TextureUnit.Texture0);
        Gl.BindTexture(TextureTarget.Texture2D, textureToDebug);
        int textureLocation0 = _shader.GetUniformLocation("texture0");
        Gl.Uniform1i(textureLocation0, 0);

        //Gl.ActiveTexture(TextureUnit.Texture1);
        //Gl.BindTexture(TextureTarget.Texture2D, _textureToDebug.TextureID);
        //int textureLocation1 = _shader.GetUniformLocation("texture1");
        //Gl.Uniform1i(textureLocation1, 1);
        //Gl.DrawArrays(BeginMode.Triangles, 0, meshData.Count);
        Gl.DrawElements(BeginMode.Triangles, meshIndices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
      }
    }

    public void CreateShader() {
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\DebugHelperVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\DebugHelperFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }

  class MergeTextures {
    private VBO<Vector3> meshData;
    private VBO<Vector3> colorData;
    private VBO<Vector2> uvParamData;
    private VBO<int> meshIndices;
    private ShaderProgram _shader;
    private static bool _updateShader = true;

    public void InitialiseData() {
      meshData = new VBO<Vector3>(new Vector3[] { new Vector3(-1, -1, 0), new Vector3(-1, 1, 0), new Vector3(1, 1, 0), new Vector3(1, -1, 0), });
      colorData = new VBO<Vector3>(new Vector3[] { new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1) });
      uvParamData = new VBO<Vector2>(new Vector2[] { new Vector2(0.0f, 0.0f), new Vector2(0.0f, 1.0f), new Vector2(1.0f, 1.0f), new Vector2(1.0f, 0.0f) });

      meshIndices = new VBO<int>(new int[] { 0, 1, 2, 2, 3, 0 }, BufferTarget.ElementArrayBuffer);
      ReadVertexShaderFromFile();
      ReadFragmentShaderFromFile();

      CreateShader();
      CreateTexture();
    }

    private static Texture _textureToDebug;
    public void CreateTexture() {
      // load a debug texture
      if (_textureToDebug == null) {
        _textureToDebug = new Texture("..\\..\\Shaders\\chessboard.png");
      }
    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public void Merge(uint texture0, uint texture1) {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      Gl.UseProgram(_shader);

      Gl.BindBufferToShaderAttribute(meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(uvParamData, _shader, "vertexUV");
      Gl.BindBuffer(meshIndices);

      Program demoprogram = Program.Instance;
      if (demoprogram != null) {
          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

          Gl.ActiveTexture(TextureUnit.Texture0);
          Gl.BindTexture(TextureTarget.Texture2D, texture0);
          int textureLocation0 = _shader.GetUniformLocation("texture0");
          Gl.Uniform1i(textureLocation0, 0);

          Gl.ActiveTexture(TextureUnit.Texture1);
          Gl.BindTexture(TextureTarget.Texture2D, texture1);
          int textureLocation1 = _shader.GetUniformLocation("texture1");
          Gl.Uniform1i(textureLocation1, 1);

          Gl.DrawElements(BeginMode.Triangles, meshIndices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
      }
    }

    public void CreateShader() {
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\MergeTexturesVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\MergeTexturesFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }

  class RenderBloom {
    private VBO<Vector3> meshData;
    private VBO<Vector3> colorData;
    private VBO<Vector2> uvParamData;
    private VBO<int> meshIndices;
    private ShaderProgram _shader;
    private static bool _updateShader = true;

    public void InitialiseData() {
      meshData = new VBO<Vector3>(new Vector3[] { new Vector3(-1, -1, 0), new Vector3(-1, 1, 0), new Vector3(1, 1, 0), new Vector3(1, -1, 0), });
      colorData = new VBO<Vector3>(new Vector3[] { new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1) });
      uvParamData = new VBO<Vector2>(new Vector2[] { new Vector2(0.0f, 0.0f), new Vector2(0.0f, 1.0f), new Vector2(1.0f, 1.0f), new Vector2(1.0f, 0.0f) });

      meshIndices = new VBO<int>(new int[] { 0, 1, 2, 2, 3, 0 }, BufferTarget.ElementArrayBuffer);
      ReadVertexShaderFromFile();
      ReadFragmentShaderFromFile();

      CreateShader();
      CreateTexture();
    }

    private static Texture _textureToDebug;
    public void CreateTexture() {
      // load a debug texture
      if (_textureToDebug == null) {
        _textureToDebug = new Texture("..\\..\\Shaders\\chessboard.png");
      }
    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public void Render(uint bloomTexture, SceneGraph<IGraphicObject> sgInstance) {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      Gl.CullFace(CullFaceMode.Front);
      Gl.UseProgram(_shader);

      Gl.BindBufferToShaderAttribute(meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(uvParamData, _shader, "vertexUV");
      Gl.BindBuffer(meshIndices);

      int renderMode = 0;
      if (bloomTexture > 0)
        renderMode = 1;
      _shader["renderMode"].SetValue(renderMode);

      Program demoprogram = Program.Instance;
      if (demoprogram != null)
      {
        if (bloomTexture != 0)
        {
          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
          Gl.ActiveTexture(TextureUnit.Texture0);
          Gl.BindTexture(TextureTarget.Texture2D, bloomTexture);
          int textureLocation0 = _shader.GetUniformLocation("image");
          Gl.Uniform1i(textureLocation0, 0);
          Gl.DrawElements(BeginMode.Triangles, meshIndices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
        } 
        else
        {
          int horizontal = 0;
          for (int i = 0; i < 10; i++) {
            horizontal = 1 - horizontal;
            Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._bloomFrameBufferName[horizontal]);
            _shader["horizontal"].SetValue(horizontal);

            Vector3 newLocation = new Vector3(0, 0, 1);
            _shader["shadow_camera_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
            _shader["view_matrix"].SetValue(Matrix4.LookAt(newLocation, Vector3.Zero, Vector3.UnitY));
            _shader["biasMatrix"].SetValue(SceneGraph<IGraphicObject>.BiasMatrix);
            _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
            Gl.ActiveTexture(TextureUnit.Texture0);
            Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._bloomTexture[1 - horizontal]);
            int textureLocation0 = _shader.GetUniformLocation("image");
            Gl.Uniform1i(textureLocation0, 0);
            Gl.DrawElements(BeginMode.Triangles, meshIndices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
          }
        }
      }
    }

    public void CreateShader() {
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\GaussianBlurVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\GaussianBlurFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }

  class HelperCube : GraphicObjectBase {
    private static bool _updateShader = true;
    public override void Render(RenderState renderStateIn, SceneGraph<IGraphicObject> sgInstance) {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      // set up the viewport and clear the previous depth and color buffers
      // make sure the shader program and texture are being used
      Gl.UseProgram(_shader);
      Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
      //Gl.BindBuffer(_meshIndices);
      Gl.BindBuffer(_meshData);

      Program demoProgram = Program.Instance;
      if (demoProgram != null) {
        Gl.Disable(EnableCap.CullFace);
        Gl.Enable(EnableCap.Blend);
        Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
        if (renderStateIn == RenderState.Shadow) {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
        } else {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
        }
        _shader["model_matrix"].SetValue(Transform);
        _shader["normalMatrix"].SetValue(NormalMatrix);

        Vector4 lightPos = new Vector4();
        foreach (var element in sgInstance.DisplayList) {
          SphereLight sphereLight = element as SphereLight;
          if (sphereLight != null) {
            lightPos = sphereLight.Transform.Transpose() * new Vector4(sphereLight._lightOrigin, 1);
          }
        }

        _shader["lightPos"].SetValue(lightPos);
        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);
      }
    }

    public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {
      STLReader stlReader = new STLReader("..\\..\\STLModel\\10mm_test_cube.stl");

      TriangleMesh[] meshArray = stlReader.ReadFile();


      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();


      double _red = 1.0, _green = 1.0, _blue = 0.0;
      for (int i = 0; i < 1/*meshArray.Length*/; i++) {
        Vector_3 vertex1 = meshArray[i].vert1;
        Vector_3 vertex2 = meshArray[i].vert2;
        Vector_3 vertex3 = meshArray[i].vert3;

        float scaleFactor =2f;
        generatedVertexData.Add(new Vector3(vertex1.x / scaleFactor, vertex1.z / scaleFactor, -1 * vertex1.y / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex2.x / scaleFactor, vertex2.z / scaleFactor, -1 * vertex2.y / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex3.x / scaleFactor, vertex3.z / scaleFactor, -1 * vertex3.y / scaleFactor));

        Vector_3 normal1 = meshArray[i].normal1;
        Vector_3 normal2 = meshArray[i].normal2;
        Vector_3 normal3 = meshArray[i].normal3;

        generatedNormalsData.Add(new Vector3(normal1.x, normal1.y, normal1.z));
        generatedNormalsData.Add(new Vector3(normal2.x, normal2.y, normal2.z));
        generatedNormalsData.Add(new Vector3(normal3.x, normal3.y, normal3.z));

        generatedColorData.Add(new Vector3(_red, _green, _blue));
        generatedColorData.Add(new Vector3(_red, _green, _blue));
        generatedColorData.Add(new Vector3(_red, _green, _blue));
      }

      _meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      CreateShader();
    }

    protected override void CreateTextures() {

    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public override void Animate(RenderState renderStateIn) {
      Matrix4 parentTransform = SceneGraph<IGraphicObject>.transformStack.Peek();
      Transform = Matrix4.Identity;
      Transform = Transform * parentTransform;

      NormalMatrix = Transform.Inverse().Transpose();
    }

    private Matrix4 NormalMatrix {
      get;
      set;
    }

    public override Vector3 Origin() {
      return new Vector3();
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\HelperCubeVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\HelperCubeFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}