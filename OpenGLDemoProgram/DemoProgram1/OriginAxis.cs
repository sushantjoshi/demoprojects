﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Runtime.InteropServices;

namespace DemoProgram1 {
  class OriginAxis : GraphicObjectBase {

    protected VBO<Vector3> _meshData1;
    protected VBO<Vector3> _colorData1;
    protected VBO<Vector3> _meshData2;
    protected VBO<Vector3> _colorData2;
    protected VBO<Vector3> _meshData3;
    protected VBO<Vector3> _colorData3;

    Matrix4 xAxisMatrix = Matrix4.CreateScaling(new Vector3(10.0f, 0.1f, 0.1f))* Matrix4.CreateTranslation(new Vector3(10.0f, 0.0f, 0.0f)) ;
    Matrix4 yAxisMatrix = Matrix4.CreateScaling(new Vector3(0.1f, 10.0f, 0.1f)) *Matrix4.CreateTranslation(new Vector3(0.0f, 10.0f, 0.0f));
    Matrix4 zAxisMatrix = Matrix4.CreateScaling(new Vector3(0.1f, 0.1f, 10.0f)) *Matrix4.CreateTranslation(new Vector3(0.0f, 0.0f, 10.0f));

    private bool _updateShader = true;
    public override void Render(RenderState renderStateIn, SceneGraph<IGraphicObject> sgInstance)
    {
      if (renderStateIn == RenderState.Shadow)
        return;
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      // set up the viewport and clear the previous depth and color buffers
      // make sure the shader program and texture are being used
      Gl.UseProgram(_shader);

      Program demoProgram = Program.Instance;
      if (demoProgram != null) {
        Gl.Disable(EnableCap.CullFace);
        Gl.Disable(EnableCap.DepthTest);
        Gl.Enable(EnableCap.Blend);
        Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
        _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);


        Gl.BindBufferToShaderAttribute(_meshData1, _shader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData1, _shader, "vertexColor");
        _shader["model_matrix"].SetValue(xAxisMatrix);
        Gl.BindBuffer(_meshData1);
        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData1.Count);

        Gl.BindBufferToShaderAttribute(_meshData2, _shader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData2, _shader, "vertexColor");
        _shader["model_matrix"].SetValue(yAxisMatrix);
        Gl.BindBuffer(_meshData2);
        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData2.Count);

        Gl.BindBufferToShaderAttribute(_meshData3, _shader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData3, _shader, "vertexColor");
        _shader["model_matrix"].SetValue(zAxisMatrix);
        Gl.BindBuffer(_meshData3);
        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData3.Count);

        Gl.Enable(EnableCap.DepthTest);
      }
    }

    public bool DebugHelper {
      get; set;
    }

    enum Axis
    {
      xAxis,
      yAxis,
      zAxis
    }
    private void CreateSingleAxis(Axis axis)
    {
      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();

      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(1.0f, -1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(1.0f, -1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(1.0f, -1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, -1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f, -1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(1.0f, 1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(1.0f, -1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f, -1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f, 1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, -1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, -1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f, -1.0f, 1.0f));

      float r = 0.0f;
      float g = 0.0f;
      float b = 0.0f;

      switch (axis)
      {
        case Axis.xAxis:
          r = 1.0f;
          break;
        case Axis.yAxis:
          g = 1.0f;
          break;
        case Axis.zAxis:
          b = 1.0f;
          break;
      }
      for (int i = 0; i < 36; i++)
      {
        generatedColorData.Add(new Vector3(r, g, b));
      }

      switch (axis) {
        case Axis.xAxis:
          _meshData1 = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
          _colorData1 = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
          break;
        case Axis.yAxis:
          _meshData2 = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
          _colorData2 = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
          break;
        case Axis.zAxis:
          _meshData3 = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
          _colorData3 = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
          break;
      }
    }

    public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel)
    {
      CreateSingleAxis(Axis.xAxis);
      CreateSingleAxis(Axis.yAxis);
      CreateSingleAxis(Axis.zAxis);

      CreateShader();

      ModelToWorldTransform = Matrix4.Identity;
      if (DebugHelper == true) {
        //Also create the model to world transform
        Matrix4 translationMatrix1 = Matrix4.CreateTranslation(new Vector3(-0.5f, -0.5f, 0));
        Matrix4 rotationMatrixAroundXAxis = Matrix4.CreateRotationX(1.57f);
        //Matrix4 translationMatrix2 = Matrix4.CreateTranslation(new Vector3(0, 0, -0.5f));

        ModelToWorldTransform = rotationMatrixAroundXAxis * translationMatrix1;// * translationMatrix2;
      }
    }

    protected override void CreateTextures() {

    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public override void Animate(RenderState renderStateIn) {
      Matrix4 parentTransform = SceneGraph<IGraphicObject>.transformStack.Peek();
      Transform = ModelToWorldTransform;
      Transform = Transform * parentTransform;
    }

    public override Vector3 Origin() {
      return new Vector3();
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\OriginAxisVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\OriginAxisFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}