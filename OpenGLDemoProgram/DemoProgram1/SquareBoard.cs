﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Runtime.InteropServices;
using  System.Drawing;

namespace DemoProgram1 {
  class SquarePlane : GraphicObjectBase {
    private bool _updateShader = true;
    private Texture _chessBoardTexture;
    public override void Render(RenderState renderStateIn, SceneGraph<IGraphicObject> sgInstance) {
      if (renderStateIn == RenderState.Bloom) {
        Gl.Disable(EnableCap.StencilTest);
        return;
      }

      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      // set up the viewport and clear the previous depth and color buffers
      // make sure the shader program and texture are being used
      Gl.UseProgram(_shader);
      Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
      Gl.BindBufferToShaderAttribute(_uvParamData, _shader, "vertexUV");
      //Gl.BindBuffer(_meshIndices);
      Gl.BindBuffer(_meshData);

      Program demoProgram = Program.Instance;
      if (demoProgram != null) {
        if (renderStateIn == RenderState.Reflection)
        {
          Gl.Enable(EnableCap.StencilTest);
          Gl.StencilMask(0xFF);
          Gl.StencilOp(StencilOp.Keep, StencilOp.Replace, StencilOp.Replace);
          Gl.StencilFunc(StencilFunction.Always, 1, 1);
        }
        else
        {
          Gl.Disable(EnableCap.StencilTest);
        }

        Gl.Disable(EnableCap.CullFace);
        Gl.Enable(EnableCap.Blend);
        Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        Vector3 newLocation = demoProgram.cameraLocation;
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
        if (renderStateIn == RenderState.Shadow) {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
        } else {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
        }

        Vector4 lightPos = new Vector4();
        foreach (var element in sgInstance.DisplayList) {
          SphereLight sphereLight = element as SphereLight;
          if (sphereLight != null) {
            lightPos = sphereLight.Transform.Transpose() * new Vector4(sphereLight._lightOrigin, 1);
          }
        }
        Vector4 cameraLocation = new Vector4(demoProgram.cameraLocation, 1.0f);
        _shader["cameraLocation"].SetValue(cameraLocation);
        _shader["lightPos"].SetValue(lightPos);
        _shader["shadow_camera_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
        _shader["model_matrix"].SetValue(Transform);
        _shader["normalMatrix"].SetValue(NormalMatrix);
        LightSpaceMatrix = SceneGraph<IGraphicObject>.ShadowMapCamera * SceneGraph<IGraphicObject>.ProjectionMatrix;
        _shader["LightSpaceMatrix"].SetValue(LightSpaceMatrix);

        Gl.ActiveTexture(TextureUnit.Texture10);
        Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._shadowTexture[1]);
        int textureLocation1 = _shader.GetUniformLocation("shadowMap");
        Gl.Uniform1i(textureLocation1, 10);

        Gl.ActiveTexture(TextureUnit.Texture5);
        Gl.BindTexture(TextureTarget.Texture2D, _chessBoardTexture.TextureID);
        int textureLocation2 = _shader.GetUniformLocation("chessBoardTexture");
        Gl.Uniform1i(textureLocation2, 5);

        Gl.ActiveTexture(TextureUnit.Texture3);
        Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._reflectionTexture[0]);
        int textureLocation3 = _shader.GetUniformLocation("reflectionMap");
        Gl.Uniform1i(textureLocation3, 3);

        //Gl.ActiveTexture(TextureUnit.Texture4);
        //Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._bloomTexture[0]);
        //int textureLocation4 = _shader.GetUniformLocation("bloomTexture");
        //Gl.Uniform1i(textureLocation4, 4);



        int renderMode = 0;
        switch (renderStateIn)
        {
          case RenderState.Default:
            renderMode = 0;
            break;
          case RenderState.Shadow:
            renderMode = 1;
            break;
          case RenderState.Reflection:
            renderMode = 2;
            break;
        }
        _shader["renderMode"].SetValue(renderMode);

        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);

        if (renderStateIn == RenderState.Reflection)
        {
          Gl.StencilFunc(StencilFunction.Equal, 1, 1);
          Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }
      }
    }

    public bool DebugHelper
    {
      get; set;
    }
    
      public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {

      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();
      List<Vector2> generatedUVParam = new List<Vector2>();

      generatedVertexData.Add(new Vector3(0, 0, 0));
      generatedVertexData.Add(new Vector3(1, 0, 0));
      generatedVertexData.Add(new Vector3(1, 1, 0));

      generatedColorData.Add(new Vector3(1.0f, 0.0f, 0.0f));
      generatedColorData.Add(new Vector3(0.0f, 1.0f, 0.0f));
      generatedColorData.Add(new Vector3(0.0f, 0.0f, 1.0f));

      generatedUVParam.Add(new Vector2(0, 0));
      generatedUVParam.Add(new Vector2(1, 0));
      generatedUVParam.Add(new Vector2(1, 1));

      generatedNormalsData.Add(new Vector3(0, 0, 1));
      generatedNormalsData.Add(new Vector3(0, 0, 1));
      generatedNormalsData.Add(new Vector3(0, 0, 1));

      generatedVertexData.Add(new Vector3(0, 0, 0));
      generatedVertexData.Add(new Vector3(1, 1, 0));
      generatedVertexData.Add(new Vector3(0, 1, 0));

      generatedColorData.Add(new Vector3(1.0f, 1.0f, 0.0f));
      generatedColorData.Add(new Vector3(1.0f, 0.0f, 1.0f));
      generatedColorData.Add(new Vector3(0.0f, 1.0f, 1.0f));

      generatedUVParam.Add(new Vector2(0, 0));
      generatedUVParam.Add(new Vector2(1, 1));
      generatedUVParam.Add(new Vector2(0, 1));

      generatedNormalsData.Add(new Vector3(0, 0, 1));
      generatedNormalsData.Add(new Vector3(0, 0, 1));
      generatedNormalsData.Add(new Vector3(0, 0, 1));

      _meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _uvParamData = new VBO<Vector2>(generatedUVParam.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      CreateShader();
      CreateTextures();

      ModelToWorldTransform = Matrix4.Identity * Matrix4.CreateTranslation(new Vector3(-0.5f, -0.5f, 0.0f))*  Matrix4.CreateScaling(new Vector3(30.0f, 30.0f, 1.0f)) * Matrix4.CreateRotationX(-1.570f);
      if (DebugHelper == true)
      {
        //Also create the model to world transform
        Matrix4 translationMatrix1 = Matrix4.Identity;//Matrix4.CreateTranslation(new Vector3(-0.5f, -0.5f, 0));
        Matrix4 rotationMatrixAroundXAxis = Matrix4.CreateRotationX(1.5707f); //Matrix4.CreateRotationX(1.57f);
        Matrix4 rotationMatrixAroundYAxis = Matrix4.CreateRotationY(1.5707f);
        Matrix4 netRotation1 = rotationMatrixAroundYAxis * rotationMatrixAroundXAxis;
        Matrix4 netRotation2 = rotationMatrixAroundXAxis * rotationMatrixAroundYAxis;

        //Matrix4 translationMatrix2 = Matrix4.CreateTranslation(new Vector3(0, 0, -0.5f));

        //ModelToWorldTransform = translationMatrix1* rotationMatrixAroundXAxis ;// * translationMatrix2;
        ModelToWorldTransform = netRotation1;
      }
      }

    protected override void CreateTextures() {
      // load a chess board texture
      if (_chessBoardTexture == null) {
        _chessBoardTexture = new Texture("..\\..\\Shaders\\chessboard.png");
      }
    }

  private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public override void Animate(RenderState renderStateIn) {
      Matrix4 parentTransform = SceneGraph<IGraphicObject>.transformStack.Peek();
      Transform = ModelToWorldTransform;
      Transform = Transform * parentTransform;
      NormalMatrix = Transform.Inverse().Transpose();
    }

    private Matrix4 LightSpaceMatrix
    {
      get;
      set;
    }
    private Matrix4 NormalMatrix {
      get;
      set;
    }


    public override Vector3 Origin() {
      return new Vector3();
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\SquarePlaneVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\SquarePlaneFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}