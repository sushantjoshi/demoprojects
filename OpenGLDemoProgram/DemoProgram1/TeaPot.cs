﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;

namespace DemoProgram1 {
  class TeaPot : GraphicObjectBase {
    private static bool _updateShader = true;
    private Matrix4 modelMatrix;

    public override void Render(RenderState renderStateIn, SceneGraph<IGraphicObject> sgInstance) {
      if (renderStateIn == RenderState.Bloom)
        return;

      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      // set up the viewport and clear the previous depth and color buffers
      // make sure the shader program and texture are being used
      Gl.UseProgram(_shader);
      Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
      //Gl.BindBuffer(_meshIndices);
      Gl.BindBuffer(_meshData);

      Program demoProgram = Program.Instance;
      if (demoProgram != null) {
        //Gl.StencilFunc(StencilFunction.Equal, 1, 1);
        Gl.Disable(EnableCap.CullFace);
        Gl.Enable(EnableCap.Blend);
        Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
        if (renderStateIn == RenderState.Shadow) {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
          //Gl.Enable(EnableCap.CullFace);
          //Gl.CullFace(CullFaceMode.Front);
        } else {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
          //Gl.Enable(EnableCap.CullFace);
          //Gl.CullFace(CullFaceMode.Back);
        }
        _shader["model_matrix"].SetValue(Transform);
        _shader["normalMatrix"].SetValue(NormalMatrix);

        Vector4 lightPos = new Vector4();
        foreach (var element in sgInstance.DisplayList) {
          SphereLight sphereLight = element as  SphereLight;
          if (sphereLight != null)
          {
            lightPos = sphereLight.Transform.Transpose() * new Vector4(sphereLight._lightOrigin, 1);
          }
        }

        _shader["lightPos"].SetValue(lightPos);
        Vector4 cameraLocation = new Vector4(demoProgram.cameraLocation, 1.0f);
        _shader["cameraLocation"].SetValue(cameraLocation);

        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);
      }
    }

    public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {
      STLReader stlReader = new STLReader("..\\..\\STLModel\\Utah_teapot.stl");//scaleFactor = 10

      TriangleMesh[] meshArray = stlReader.ReadFile();


      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();


      double _red = 1.0, _green = 1.0, _blue = 0.0;
      for (int i = 0; i < meshArray.Length; i++) {
        Vector_3 vertex1 = meshArray[i].vert1;
        Vector_3 vertex2 = meshArray[i].vert2;
        Vector_3 vertex3 = meshArray[i].vert3;

        float scaleFactor = 1f;
        generatedVertexData.Add(new Vector3(vertex1.x / scaleFactor, vertex1.y / scaleFactor, 1*vertex1.z / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex2.x / scaleFactor, vertex2.y / scaleFactor, 1*vertex2.z / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex3.x / scaleFactor, vertex3.y / scaleFactor, 1*vertex3.z / scaleFactor));

        Vector_3 normal1 = meshArray[i].normal1;
        Vector_3 normal2 = meshArray[i].normal2;
        Vector_3 normal3 = meshArray[i].normal3;

        generatedNormalsData.Add(new Vector3(normal1.x, normal1.y, normal1.z));
        generatedNormalsData.Add(new Vector3(normal2.x, normal2.y, normal2.z));
        generatedNormalsData.Add(new Vector3(normal3.x, normal3.y, normal3.z));

        generatedColorData.Add(new Vector3(_red, _green,_blue));
        generatedColorData.Add(new Vector3(_red, _green,_blue));
        generatedColorData.Add(new Vector3(_red, _green,_blue));

        //if (_red > 0)
        //{
        //  _red = 0.0;
        //  _blue = 1.0;
        //  _green = 0.0;
        //}
        //else if (_blue > 0)
        //{
        //  _red = 0.0;
        //  _blue = 0.0;
        //  _green = 1.0;
        //} 
        //else if (_green > 0)
        //{
        //  _red = 1.0;
        //  _blue = 0.0;
        //  _green = 0.0;
        //}

      }

      modelMatrix = Matrix4.CreateRotationX(-1.57f);
      _meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      CreateShader();
    }

    protected override void CreateTextures() {

    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public override void Animate(RenderState renderStateIn) {
      Matrix4 parentTransform = SceneGraph<IGraphicObject>.transformStack.Peek();
      Transform = Matrix4.Identity;
      if (renderStateIn == RenderState.Reflection)
      {
        Transform = modelMatrix* Matrix4.CreateScaling(new Vector3(1.0f, -1.0f, 1.0f));// * Matrix4.CreateRotationY(rotationAngle);//* RotationTransform * RadialDistance * parentTransform;
        Gl.Enable(EnableCap.CullFace);
        Gl.CullFace(CullFaceMode.Front);
      }
      else
      {
        Transform = modelMatrix;// * Matrix4.CreateRotationY(rotationAngle);//* RotationTransform * RadialDistance * parentTransform;
        Gl.Enable(EnableCap.CullFace);
        Gl.CullFace(CullFaceMode.Back);
      }
      Transform = Transform * parentTransform;

      NormalMatrix = Transform.Inverse().Transpose();
    }

    private Matrix4 NormalMatrix
    {
      get;
      set;
    }

    public override Vector3 Origin() {
      return new Vector3();
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\TeaPotVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\TeaPotFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}