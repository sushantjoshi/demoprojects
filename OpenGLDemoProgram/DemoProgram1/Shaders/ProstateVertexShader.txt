#version 130

in vec3 vertexPosition;
in vec3 vertexColor;
in vec3 normals;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform mat4 normal_matrix;

out vec3 color;
out vec4 N;
out vec4 I;
out vec4 Cs;

void main(void)
{
        vec4 P = view_matrix * model_matrix * vec4(vertexPosition, 1);	
        vec3 temp = P.xyz - vec3 (0, 0, 0);
        I  = vec4(temp, 0);
        N  = vec4(normals, 0);
        N =  normal_matrix *N;
        color = vec3(N.x, N.y, N.z);
        Cs =  vec4(color, 1.0);
        gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1);
}