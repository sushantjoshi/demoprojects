#version 130

in vec3 vertexPosition;
in vec3 vertexColor;
in vec3 normals;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform mat4 normalMatrix;

out vec3 color;
out vec4 modifiedNormals;
out vec4 WorldFragPos;  

void main(void)
{
    color = vertexColor;
	
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1);
	modifiedNormals = normalize(normalMatrix * vec4(normals, 0));
	WorldFragPos  = model_matrix * vec4(vertexPosition, 1);
}