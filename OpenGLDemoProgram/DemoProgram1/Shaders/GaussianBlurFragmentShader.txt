#version 130

out vec4 FragColor;
in vec3 color;
in vec4 ShadowCoord;
in vec2 uv;
in vec2 vertexUV;
in vec4 modifiedNormals;
in vec4 WorldFragPos;

uniform vec4 lightPos;
uniform mat4 LightSpaceMatrix;

uniform sampler2D shadowMap;
uniform sampler2D chessBoardTexture;
uniform sampler2D reflectionMap;

uniform int renderMode;
uniform vec4 cameraLocation;

in vec2 TexCoords;

uniform sampler2D image;
  
uniform int horizontal;
uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

void main()
{
	//if(renderMode >0)
	//	return;
		
	//FragColor = texture2D(image, uv);
	//if(FragColor.w < 0.5)
	//	FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	//else
	//	FragColor = vec4(0.0, 1.0, 0.0, 1.0);
	//FragColor = vec4(0.0, 1.0, 0.0, 1.0);
	//return;
	
	vec2 tex_offset = 1.0 / textureSize(image, 0); // gets size of single texel
    vec3 result = texture2D(image, uv).rgb * weight[0]; // current fragment's contribution
    
	if(horizontal==1)
    {
        for(int i = 1; i < 5; ++i)
        {
            result += texture2D(image, uv + vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
            result += texture2D(image, uv - vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
        }
    }
    else
    {
        for(int i = 1; i < 5; ++i)
        {
            result += texture2D(image, uv + vec2(0.0, tex_offset.y * i)).rgb * weight[i];
            result += texture2D(image, uv - vec2(0.0, tex_offset.y * i)).rgb * weight[i];
        }
    }

    FragColor = vec4(result, 1.0);
	return;	
}