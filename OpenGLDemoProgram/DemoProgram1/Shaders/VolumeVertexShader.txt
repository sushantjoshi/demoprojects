#version 130

in vec3 vertexPosition;
in vec3 vertexColor;
in vec2 vertexUV;

//uniform mat4 view_matrix;
//uniform mat4 projection_matrix;
//uniform mat4 model_matrix;

out vec3 color;
out vec2 uv;

void main(void)
{
    uv = vertexUV;
    color = vertexColor;
    //gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1);
	gl_Position = vec4(vertexPosition, 1);
}