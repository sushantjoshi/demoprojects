#version 130

in vec3 color;
out vec4 fragment;

in vec4 N;
in vec4 I;
in vec4 Cs;
in float zDepth;

void main(void)
{	
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	gl_FragColor = vec4(zDepth, zDepth, 0.0, 1.0);
}