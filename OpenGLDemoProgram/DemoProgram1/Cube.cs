﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Runtime.InteropServices;

namespace DemoProgram1 {
  class Cube : GraphicObjectBase {
    private bool _updateShader = true;
    public override void Render(RenderState renderStateIn, SceneGraph<IGraphicObject> sgInstance) {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      // set up the viewport and clear the previous depth and color buffers
      // make sure the shader program and texture are being used
      Gl.UseProgram(_shader);
      Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
      //Gl.BindBuffer(_meshIndices);
      Gl.BindBuffer(_meshData);

      Program demoProgram = Program.Instance;
      if (demoProgram != null) {
        Gl.Disable(EnableCap.CullFace);
        Gl.Enable(EnableCap.Blend);
        Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        Vector3 newLocation = demoProgram.cameraLocation;
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);

        if (renderStateIn == RenderState.Shadow) {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
        }
        else
        {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
        }
        
        _shader["model_matrix"].SetValue(Transform);
        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);
      }
    }

    public bool DebugHelper {
      get; set;
    }

    public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {

      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();

      generatedVertexData.Add(new Vector3(-1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(1.0f,-1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(1.0f,-1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(-1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(1.0f,-1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f,-1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f,-1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(1.0f, 1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(1.0f,-1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f,-1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f, 1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f,-1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f,-1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));

      generatedVertexData.Add(new Vector3(1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(-1.0f, 1.0f, 1.0f));
      generatedVertexData.Add(new Vector3(1.0f,-1.0f, 1.0f));

      generatedColorData.Add(new Vector3(0.583f, 0.771f, 0.014f));
      generatedColorData.Add(new Vector3(0.609f, 0.115f, 0.436f));
      generatedColorData.Add(new Vector3(0.327f, 0.483f, 0.844f));

      generatedColorData.Add(new Vector3(0.822f, 0.569f, 0.201f));
      generatedColorData.Add(new Vector3(0.435f, 0.602f, 0.223f));
      generatedColorData.Add(new Vector3(0.310f, 0.747f, 0.185f));

      generatedColorData.Add(new Vector3(0.597f, 0.770f, 0.761f));
      generatedColorData.Add(new Vector3(0.559f, 0.436f, 0.730f));
      generatedColorData.Add(new Vector3(0.359f, 0.583f, 0.152f));

      generatedColorData.Add(new Vector3(0.483f, 0.596f, 0.789f));
      generatedColorData.Add(new Vector3(0.559f, 0.861f, 0.639f));
      generatedColorData.Add(new Vector3(0.195f, 0.548f, 0.859f));

      generatedColorData.Add(new Vector3(0.014f, 0.184f, 0.576f));
      generatedColorData.Add(new Vector3(0.771f, 0.328f, 0.970f));
      generatedColorData.Add(new Vector3(0.406f, 0.615f, 0.116f));

      generatedColorData.Add(new Vector3(0.676f, 0.977f, 0.133f));
      generatedColorData.Add(new Vector3(0.971f, 0.572f, 0.833f));
      generatedColorData.Add(new Vector3(0.140f, 0.616f, 0.489f));

      generatedColorData.Add(new Vector3(0.997f, 0.513f, 0.064f));
      generatedColorData.Add(new Vector3(0.945f, 0.719f, 0.592f));
      generatedColorData.Add(new Vector3(0.543f, 0.021f, 0.978f));

      generatedColorData.Add(new Vector3(0.279f, 0.317f, 0.505f));
      generatedColorData.Add(new Vector3(0.167f, 0.620f, 0.077f));
      generatedColorData.Add(new Vector3(0.347f, 0.857f, 0.137f));

      generatedColorData.Add(new Vector3(0.055f, 0.953f, 0.042f));
      generatedColorData.Add(new Vector3(0.714f, 0.505f, 0.345f));
      generatedColorData.Add(new Vector3(0.783f, 0.290f, 0.734f));

      generatedColorData.Add(new Vector3(0.722f, 0.645f, 0.174f));
      generatedColorData.Add(new Vector3(0.302f, 0.455f, 0.848f));
      generatedColorData.Add(new Vector3(0.225f, 0.587f, 0.040f));

      generatedColorData.Add(new Vector3(0.517f, 0.713f, 0.338f));
      generatedColorData.Add(new Vector3(0.053f, 0.959f, 0.120f));
      generatedColorData.Add(new Vector3(0.393f, 0.621f, 0.362f));

      generatedColorData.Add(new Vector3(0.673f, 0.211f, 0.457f));
      generatedColorData.Add(new Vector3(0.820f, 0.883f, 0.371f));
      generatedColorData.Add(new Vector3(0.982f, 0.099f, 0.879f));

      _meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      CreateShader();

      ModelToWorldTransform = Matrix4.Identity;
      if (DebugHelper == true) {
        //Also create the model to world transform
        Matrix4 translationMatrix1 = Matrix4.CreateTranslation(new Vector3(-0.5f, -0.5f, 0));
        Matrix4 rotationMatrixAroundXAxis = Matrix4.CreateRotationX(1.57f);
        //Matrix4 translationMatrix2 = Matrix4.CreateTranslation(new Vector3(0, 0, -0.5f));

        ModelToWorldTransform = rotationMatrixAroundXAxis * translationMatrix1;// * translationMatrix2;
      }

      //List<Vector3> generatedVertexData = new List<Vector3>();
      //List<Vector3> generatedColorData = new List<Vector3>();
      //List<Vector3> generatedNormalsData = new List<Vector3>();

      //generatedVertexData.Add(new Vector3(-1, 0, -1));
      //generatedVertexData.Add(new Vector3(-1, 0, 1));
      //generatedVertexData.Add(new Vector3(1, 0, 1));

      //generatedColorData.Add(new Vector3(1.0f, 0.0f, 0.0f));
      //generatedColorData.Add(new Vector3(1.0f, 0.0f, 0.0f));
      //generatedColorData.Add(new Vector3(1.0f, 0.0f, 0.0f));

      //generatedVertexData.Add(new Vector3(-1, 0, -1));
      //generatedVertexData.Add(new Vector3(1, 0, 1));
      //generatedVertexData.Add(new Vector3(1, 0, -1));

      //generatedColorData.Add(new Vector3(0.0f, 1.0f, 0.0f));
      //generatedColorData.Add(new Vector3(0.0f, 1.0f, 0.0f));
      //generatedColorData.Add(new Vector3(0.0f, 1.0f, 0.0f));

      //_meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      //_normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      //_colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      //CreateShader();
    }

    protected override void CreateTextures() {

    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public override void Animate(RenderState renderStateIn) {
      Matrix4 parentTransform = SceneGraph<IGraphicObject>.transformStack.Peek();
      Transform = ModelToWorldTransform;
      Transform = Transform * parentTransform;
    }

    public override Vector3 Origin() {
      return new Vector3();
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\TeaPotVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\TeaPotFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}