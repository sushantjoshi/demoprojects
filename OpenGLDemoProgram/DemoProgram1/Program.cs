﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.CompilerServices;
using Tao.FreeGlut;

namespace DemoProgram1 {
  class Program {
    public Stack<Matrix4> _transformStack = new Stack<Matrix4>();
    public static int width = 1000, height = 800;

    private static arcball arcBall;

    private Matrix4f LastTransformation = new Matrix4f();
    private Matrix4f ThisTransformation = new Matrix4f();
    static SceneGraph<IGraphicObject> _sceneGraph;

    public uint[] _shadowFrameBufferName = new uint[1];
    public uint[] _shadowTexture = new uint[2];

    public uint[] _reflectionFrameBufferName = new uint[1];
    public uint[] _reflectionTexture = new uint[2];

    public uint[] _bloomFrameBufferName = new uint[2];
    public uint[] _bloomTexture = new uint[2];

    public uint[] _dummyFrameBufferName = new uint[1];
    public uint[] _dummyFrameBufferTexture = new uint[2];

    static public Program Instance {
      get;
      set;
    }

    private void InitialiseProgram() {
      _transformStack.Push(Matrix4.Identity);
      LastTransformation.SetIdentity(); // Reset Rotation
      ThisTransformation.SetIdentity(); // Reset Rotation

      // create an OpenGL window
      Glut.glutInit();
      Glut.glutSetOption(Glut.GLUT_MULTISAMPLE, 8);
      Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH | Glut.GLUT_MULTISAMPLE);
      //Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
      Glut.glutInitWindowSize(width, height);
      Glut.glutCreateWindow("testing");

      // provide the Glut callbacks that are necessary for running this tutorial
      Glut.glutIdleFunc(OnRenderFrame);
      Glut.glutCloseFunc(OnClose);
      Glut.glutMouseFunc(OnMouse);
      Glut.glutMotionFunc(OnMove);

      //Set up stuff for shadow map texture
      Gl.GenFramebuffers(1, _shadowFrameBufferName);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _shadowFrameBufferName[0]);

      Gl.GenTextures(2, _shadowTexture);

      Gl.BindTexture(TextureTarget.Texture2D, _shadowTexture[0]);
      Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, width, height, 0, PixelFormat.Rgb, PixelType.Float, System.IntPtr.Zero);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);

      Gl.BindTexture(TextureTarget.Texture2D, _shadowTexture[1]);
      Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent32, width, height, 0, PixelFormat.DepthComponent, PixelType.Float, System.IntPtr.Zero);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);

      Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, _shadowTexture[0], 0);
      Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, _shadowTexture[1], 0);

      //set up the reflection map texture
      Gl.GenFramebuffers(1, _reflectionFrameBufferName);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _reflectionFrameBufferName[0]);

      Gl.GenTextures(2, _reflectionTexture);

      Gl.BindTexture(TextureTarget.Texture2D, _reflectionTexture[0]);
      Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, width, height, 0, PixelFormat.Rgb, PixelType.Float, System.IntPtr.Zero);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);


      Gl.BindTexture(TextureTarget.Texture2D, _reflectionTexture[1]);
      Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Depth24Stencil8, width, height, 0, PixelFormat.DepthStencil, PixelType.UnsignedInt248, System.IntPtr.Zero);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);

      Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, _reflectionTexture[0], 0);
      Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthStencilAttachment, _reflectionTexture[1], 0);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

      //setup textures for bloom effect
      Gl.GenFramebuffers(2, _bloomFrameBufferName);
      Gl.GenTextures(2, _bloomTexture);

      for (int i = 0; i < 2; i++) {
        Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _bloomFrameBufferName[i]);
        Gl.BindTexture(TextureTarget.Texture2D, _bloomTexture[i]);
        Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.Float, System.IntPtr.Zero);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
        Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, _bloomTexture[i], 0);
        Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
      }

      //set up a dummy frame buffer
      Gl.GenFramebuffers(1, _dummyFrameBufferName);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _dummyFrameBufferName[0]);

      Gl.GenTextures(2, _dummyFrameBufferTexture);

      Gl.BindTexture(TextureTarget.Texture2D, _dummyFrameBufferTexture[0]);
      Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, width, height, 0, PixelFormat.Rgb, PixelType.Float, System.IntPtr.Zero);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);


      Gl.BindTexture(TextureTarget.Texture2D, _dummyFrameBufferTexture[1]);
      Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Depth24Stencil8, width, height, 0, PixelFormat.DepthStencil, PixelType.UnsignedInt248, System.IntPtr.Zero);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
      Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);

      FramebufferErrorCode frameBufferStatus1 = Gl.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
      Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, _dummyFrameBufferTexture[0], 0);
      FramebufferErrorCode frameBufferStatus2 = Gl.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
      Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthStencilAttachment, _dummyFrameBufferTexture[1], 0);
      FramebufferErrorCode frameBufferStatus3 = Gl.CheckFramebufferStatus(FramebufferTarget.Framebuffer);

      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

      Gl.Enable(EnableCap.DepthTest);
      Gl.Enable(EnableCap.Multisample);
      Gl.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);
      var test = Glut.glutGet(Glut.GLUT_MULTISAMPLE);

      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

      arcBall = new arcball(width, height);
      arcBall.setBounds((float)width, (float)height); // Update mouse bounds for arcball

      TeaPot teaPot = new TeaPot();
      SquarePlane squarePlane = new SquarePlane();
      squarePlane.DebugHelper = false;
      SquarePlane squarePlaneNew = new SquarePlane();
      squarePlaneNew.DebugHelper = true;

      OriginAxis originAxis = new OriginAxis();
      originAxis.InitialiseData(0.5f, 0.5f, 0.5f, 2);

      SphereLight sphereLight = new SphereLight();
      teaPot.InitialiseData(0.5f, 0.5f, 0.5f, 2);
      squarePlane.InitialiseData(0.5f, 0.5f, 0.5f, 2);
      squarePlaneNew.InitialiseData(0.5f, 0.5f, 0.5f, 2);
      sphereLight.InitialiseData(0.5f, 0.5f, 0.5f, 2);
      _sceneGraph = new SceneGraph<IGraphicObject>();
      //Cube cube = new Cube();
      //cube.InitialiseData(0.5f, 0.5f, 0.5f, 2);

      //HelperCube helperCube = new HelperCube();
      //helperCube.InitialiseData(0.5f, 0.5f, 0.5f, 2);
      //_sceneGraph.AddToDisplayList(helperCube);
      _sceneGraph.AddToDisplayList(squarePlane);
      _sceneGraph.AddToDisplayList(teaPot);
      _sceneGraph.AddToDisplayList(sphereLight);
      //_sceneGraph.AddToDisplayList(cube);

      //_sceneGraph.AddToDisplayList(originAxis);

      _renderTextureToScreenDebugObject = new RenderTextureToScreenDebugObject();
      _renderTextureToScreenDebugObject.InitialiseData();
      Glut.glutMainLoop();
    }

    static void Main(string[] args) {
      Instance = new Program();
      Instance.InitialiseProgram();
    }

    private static void OnClose() {

    }

    private enum mouseState {
      None,
      Pan,
      Zoom,
      Rotate
    }
    private static mouseState _mouseState = mouseState.None;
    private void OnMouse(int button, int state, int x, int y) {
      if (button == Glut.GLUT_LEFT_BUTTON) {
        _mouseState = mouseState.Rotate;
        System.Drawing.Point MousePt = new System.Drawing.Point(x, y);
        arcBall.click(MousePt); // Update Start Vector And Prepare For Dragging
        LastTransformation.set_Renamed(ThisTransformation); // Set Last Static Rotation To Last Dynamic One           
      } else if (button == Glut.GLUT_MIDDLE_BUTTON) {
        _mouseState = mouseState.Zoom;
        mouseZoomStart = new Vector2(x, y);
      }
    }

    public Vector3 cameraYAxis = new Vector3(0, 1, 0);
    public Vector3 cameraLocation = new Vector3(0, 0, 1);

    private Vector2 mouseZoomStart = new Vector2(0, 0);
    private float _cameraDistance = 35;
    private void OnMove(int x, int y) {
      if (_mouseState == mouseState.Rotate) {
        Quat4f ThisQuat = new Quat4f();
        System.Drawing.Point MousePt = new System.Drawing.Point(x, y);
        arcBall.drag(MousePt, ThisQuat); // Update End Vector And Get Rotation As Quaternion

        ThisTransformation.Pan = new Vector3f(0, 0, 0);
        ThisTransformation.Scale = 1.0f;
        ThisTransformation.Rotation = ThisQuat;
        ThisTransformation.MatrixMultiply(ThisTransformation, LastTransformation);

        float[] cameraRotation = new float[16];
        ThisTransformation.get_Renamed(cameraRotation);
        Matrix4 cameraRotationMatrix = new Matrix4(cameraRotation);
        cameraLocation = new Vector3(0, 0, _cameraDistance);
        cameraLocation = cameraRotationMatrix * cameraLocation;

        cameraYAxis = cameraRotationMatrix * new Vector3(0, 1, 0);
      } else if (_mouseState == mouseState.Zoom) {
        Vector2 mouseZoomEnd = new Vector2(x, y);
        float increment = (mouseZoomStart - mouseZoomEnd).Y;

        _cameraDistance = Convert.ToSingle(_cameraDistance + increment * 0.0005);

        if (_cameraDistance > 35)
          _cameraDistance = 35;

        if (_cameraDistance < 3)
          _cameraDistance = 3;

        Quat4f ThisQuat = new Quat4f();
        System.Drawing.Point MousePt = new System.Drawing.Point(x, y);
        arcBall.drag(MousePt, ThisQuat); // Update End Vector And Get Rotation As Quaternion

        ThisTransformation.Pan = new Vector3f(0, 0, 0);
        ThisTransformation.Scale = 1.0f;
        ThisTransformation.Rotation = ThisQuat;
        ThisTransformation.MatrixMultiply(ThisTransformation, LastTransformation);

        float[] cameraRotation = new float[16];
        ThisTransformation.get_Renamed(cameraRotation);
        Matrix4 cameraRotationMatrix = new Matrix4(cameraRotation);
        cameraLocation = new Vector3(0, 0, _cameraDistance);
        cameraLocation = cameraRotationMatrix * cameraLocation;
        cameraYAxis = cameraRotationMatrix * new Vector3(0, 1, 0);
      }
    }

    private RenderTextureToScreenDebugObject _renderTextureToScreenDebugObject = null;

    static int debugWhichTexture = -1;
    private void OnRenderFrame() {
      // set up the viewport and clear the previous depth and color buffers
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
      Gl.ClearColor(0.3f, 0.3f, 0.3f, 0.0f);

      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
      _sceneGraph.RenderComplete();

      //debugWhichTexture = 5;
      if (_renderTextureToScreenDebugObject != null && debugWhichTexture > -1)
      {
        Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
        //_renderTextureToScreenDebugObject.CreateTextures();
        _renderTextureToScreenDebugObject.Render((uint)debugWhichTexture, _sceneGraph);
      }
      Glut.glutSwapBuffers();
    }
  }
}
