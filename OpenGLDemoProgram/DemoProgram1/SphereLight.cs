﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;

namespace DemoProgram1 {
  class SphereLight : GraphicObjectBase {
    private static bool _updateShader = true;
    private Matrix4 modelMatrix;
    public override void Render(RenderState renderStateIn, SceneGraph<IGraphicObject> sgInstance)
    {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      // set up the viewport and clear the previous depth and color buffers
      // make sure the shader program and texture are being used
      Gl.UseProgram(_shader);
      Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
      Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
      Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
      //Gl.BindBuffer(_meshIndices);
      Gl.BindBuffer(_meshData);

      Program demoProgram = Program.Instance;
      if (demoProgram != null) {

        int renderMode = 0;
        switch (renderStateIn) {
          case RenderState.Default:
            renderMode = 0;
            break;
          case RenderState.Shadow:
            renderMode = 1;
            break;
          case RenderState.Reflection:
            renderMode = 2;
            break;
          case RenderState.Bloom:
            renderMode = 3;
            break;
        }
        _shader["renderMode"].SetValue(renderMode);

        Gl.Disable(EnableCap.CullFace);
        Gl.Enable(EnableCap.Blend);
        Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        Vector3 newLocation = demoProgram.cameraLocation;
        _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
        if (renderStateIn == RenderState.Shadow)
        {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ShadowMapCamera);
        } 
        else
        {
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
        }
        _shader["model_matrix"].SetValue(Transform);

        Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);
      }
    }

    private float centroidX;
    private float centroidY;
    private float centroidZ;

    public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {
      STLReader stlReader = new STLReader("..\\..\\STLModel\\Bucky_C_50mm.stl");//scaleFactor = 10

      TriangleMesh[] meshArray = stlReader.ReadFile();


      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();

      float accumulatedX = 0.0f;
      int numOfXDataPoints = 0;

      float accumulatedY = 0.0f;
      int numOfYDataPoints = 0;

      float accumulatedZ = 0.0f;
      int numOfZDataPoints = 0;

      float minX = float.MaxValue;
      float minY = float.MaxValue;
      float minZ = float.MaxValue;
      float maxX = float.MinValue;
      float maxY = float.MinValue;
      float maxZ = float.MinValue;

      float delta = 0.0f;
      float _red = 1.0f, _blue = 0.0f, _green = 1.0f;
      for (int i = 0; i < meshArray.Length; i++) {
        Vector_3 vertex1 = meshArray[i].vert1;
        Vector_3 vertex2 = meshArray[i].vert2;
        Vector_3 vertex3 = meshArray[i].vert3;

        if (minX > vertex1.x)
          minX = vertex1.x;
        if (minX > vertex2.x)
          minX = vertex2.x;
        if (minX > vertex3.x)
          minX = vertex3.x;

        if (minY > vertex1.y)
          minY = vertex1.y;
        if (minY > vertex2.y)
          minY = vertex2.y;
        if (minZ > vertex3.z)
          minZ = vertex3.z;

        if (minZ > vertex1.x)
          minZ = vertex1.x;
        if (minZ > vertex2.x)
          minZ = vertex2.x;
        if (minZ > vertex3.x)
          minZ = vertex3.x;

        if (maxX < vertex1.x)
          maxX = vertex1.x;
        if (maxX < vertex2.x)
          maxX = vertex2.x;
        if (maxX < vertex3.x)
          maxX = vertex3.x;

        if (maxY < vertex1.y)
          maxY = vertex1.y;
        if (maxY < vertex2.y)
          maxY = vertex2.y;
        if (maxZ < vertex3.z)
          maxZ = vertex3.z;

        if (maxZ < vertex1.x)
          maxZ = vertex1.x;
        if (maxZ < vertex2.x)
          maxZ = vertex2.x;
        if (maxZ < vertex3.x)
          maxZ = vertex3.x;

        accumulatedX = vertex1.x + vertex2.x + vertex3.x;
        numOfXDataPoints = numOfXDataPoints + 3;

        accumulatedY = vertex1.y + vertex2.y + vertex3.y;
        numOfYDataPoints = numOfYDataPoints + 3; ;

        accumulatedZ = vertex1.z + vertex2.z + vertex3.z; ;
        numOfZDataPoints = numOfZDataPoints+3;

        float scaleFactor = 0.1f;

        generatedVertexData.Add(new Vector3(vertex1.x / scaleFactor, vertex1.y / scaleFactor + 1.5f*delta, vertex1.z / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex2.x / scaleFactor, vertex2.y / scaleFactor + 1.5f*delta, vertex2.z / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex3.x / scaleFactor, vertex3.y / scaleFactor + 1.5f*delta, vertex3.z / scaleFactor));

        Vector_3 normal1 = meshArray[i].normal1;
        Vector_3 normal2 = meshArray[i].normal2;
        Vector_3 normal3 = meshArray[i].normal3;

        generatedNormalsData.Add(new Vector3(normal1.x, normal1.y, normal1.z));
        generatedNormalsData.Add(new Vector3(normal2.x, normal2.y, normal2.z));
        generatedNormalsData.Add(new Vector3(normal3.x, normal3.y, normal3.z));

        generatedColorData.Add(new Vector3(_red,  _green,_blue));
        generatedColorData.Add(new Vector3(_red,  _green,_blue));
        generatedColorData.Add(new Vector3(_red,  _green, _blue));

        //if (_red > 0) {
        //  _red = 0.0;
        //  _blue = 1.0;
        //  _green = 0.0;
        //} else if (_blue > 0) {
        //  _red = 0.0;
        //  _blue = 0.0;
        //  _green = 1.0;
        //} else if (_green > 0) {
        //  _red = 1.0;
        //  _blue = 0.0;
        //  _green = 0.0;
        //}
      }

      centroidX = (minX + maxX) / 2;
      centroidY = (minY + maxY) / 2;
      centroidZ = (minZ + maxZ) / 2;
      _lightOrigin = new Vector3(centroidX, centroidY, centroidZ);
      modelMatrix = Matrix4.CreateTranslation(new Vector3(-1 * centroidX, -1 * centroidY, -1 * centroidZ)) *
                    Matrix4.CreateScaling(new Vector3(0.01f, 0.01f, 0.01f)) *
                    Matrix4.CreateTranslation(new Vector3(0, 15.0f, 0)) * Matrix4.CreateTranslation(new Vector3(18.0f, 0, 0));

      float _X = accumulatedX / numOfXDataPoints;
      float _Y = accumulatedY / numOfYDataPoints;
      float _Z = accumulatedZ / numOfZDataPoints;

      _meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      CreateShader();
    }



    protected override void CreateTextures() {

    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    float rotationStep = 0.002f;
    float rotationAngle = 0f;
    private float lightSourceHeight = 10.0f;
    private float heightDelta = 0.04f;
    public override void Animate(RenderState renderStateIn) {
      //Transform = Matrix4.Identity;
      //return;
      rotationAngle = rotationAngle + rotationStep;

      lightSourceHeight = lightSourceHeight + heightDelta;
      if (lightSourceHeight > 16.05f)
        heightDelta = heightDelta * -1;
      if(lightSourceHeight < 9.05f)
        heightDelta = heightDelta * -1;

      modelMatrix = Matrix4.CreateTranslation(new Vector3(-1 * centroidX, -1 * centroidY, -1 * centroidZ)) *
                    Matrix4.CreateScaling(new Vector3(0.01f, 0.01f, 0.01f)) *
                    Matrix4.CreateTranslation(new Vector3(0, lightSourceHeight, 0)) * Matrix4.CreateTranslation(new Vector3(18.0f, 0, 0));

      if (renderStateIn == RenderState.Reflection) {
        Transform = modelMatrix * Matrix4.CreateRotationY(rotationAngle) * Matrix4.CreateScaling(new Vector3(1.0f, -1.0f, 1.0f));
      }
      else
      {
        Transform = modelMatrix* Matrix4.CreateRotationY(rotationAngle);
      }
    }

    public Vector3 _lightOrigin;
    public override Vector3 Origin()
    {
      return _lightOrigin;
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\SphereLightVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\SphereLightFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}