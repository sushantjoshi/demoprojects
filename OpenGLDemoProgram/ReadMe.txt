Demo OpenGL project

A demo OpenGL project to learn/experiment the following aspects of OpenGL/Computer graphics. The project is written in C# using OpenTK wrapper for OpenGL


1. Basic lighting (ambient + diffuse + specular) applied to the teapot and floor. The light source itself is represented by a soccer ball shaped geometry that rotates around the teapot.


2. Bloom effect applied to the rotating light source. The bloom effect is intended to provide a glow effect to the light source.


3. Usage of shadow maps: The teapot casts a shadow onto the planer surface on which it rests. The shadow is calculated in real-time.


4. Reflections: The plane on which the teapot rests is reflective in nature and reflections of the teapot and the light source can be seen on this plane.


5. UV mapping of texture: A check board texture is applied on the plane on which the teapot rests.


6. Arc ball movement: The mouse is used to rotate the entire scene using the concept of arc ball (the code for the arc ball was borrowed from an online tutorial and modified to suit my needs)
