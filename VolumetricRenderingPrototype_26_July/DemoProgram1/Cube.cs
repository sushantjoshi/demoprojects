﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;

namespace DemoProgram1 {
  class Cube: GraphicObjectBase
  {
    protected VBO<Vector3> _meshDataChild;
    protected VBO<Vector3> _colorDataChild;
    protected VBO<Vector3> _normalsDataChild;
    protected VBO<Vector2> _uvParamDataChild;
    protected VBO<int> _meshIndicesChild;
    protected ShaderProgram _volumeShader;

    private bool frontFace = true;
    private static bool _updateShader = true;
    public override void Render(RenderState renderStateIn) {
      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();

          ReadVolumeVertexShaderFromFile();
          ReadVolumetricFragmentShaderFromFile();
          ReInitVolumeShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      if (renderStateIn == RenderState.Entry) {
        // set up the viewport and clear the previous depth and color buffers
        // make sure the shader program and texture are being used
        Gl.UseProgram(_shader);
        Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
        Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
        //Gl.BindBuffer(_meshIndices);
        Gl.BindBuffer(_meshData);

        Program demoProgram = Program.Instance;
        if (demoProgram != null) {
          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._entryExitFrameBuffer[0]);
          Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._entryExitRenderTarget[0]);
          Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, Program.Instance._entryExitRenderTarget[0], 0);
          Gl.Viewport(0, 0, Program.width, Program.height);
          Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

          Gl.Enable(EnableCap.CullFace);
          Gl.CullFace(CullFaceMode.Back);

          _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
          _shader["model_matrix"].SetValue(Transform);
          Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);

          Gl.BindTexture(TextureTarget.Texture2D, 0);
          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
      }
      else if (renderStateIn == RenderState.Exit)
      {
        Gl.UseProgram(_shader);
        Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
        Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
        //Gl.BindBuffer(_meshIndices);
        Gl.BindBuffer(_meshData);

        Program demoProgram = Program.Instance;
        if (demoProgram != null) {
          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, Program.Instance._entryExitFrameBuffer[0]);
          Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._entryExitRenderTarget[1]);
          Gl.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, Program.Instance._entryExitRenderTarget[1], 0);
          Gl.Viewport(0, 0, Program.width, Program.height);
          Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

          Gl.Enable(EnableCap.CullFace);
          Gl.CullFace(CullFaceMode.Front);

          _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
          _shader["model_matrix"].SetValue(Transform);
          Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);

          Gl.BindTexture(TextureTarget.Texture2D, 0);
          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
      } 
      else if(renderStateIn == RenderState.Default)
      {
        // set up the viewport and clear the previous depth and color buffers
        // make sure the shader program and texture are being used
        Gl.UseProgram(_shader);
        Gl.BindBufferToShaderAttribute(_meshData, _shader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData, _shader, "vertexColor");
        Gl.BindBufferToShaderAttribute(_normalsData, _shader, "normals");
        //Gl.BindBuffer(_meshIndices);
        Gl.BindBuffer(_meshData);

        Program demoProgram = Program.Instance;
        if (demoProgram != null) {
          Gl.Disable(EnableCap.CullFace);
          Gl.Disable(EnableCap.DepthTest);
          Gl.Enable(EnableCap.Blend);
          Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
          Vector3 newLocation = demoProgram.cameraLocation;
          _shader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
          _shader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
          _shader["model_matrix"].SetValue(Transform);

          if (frontFace) {
            Gl.Enable(EnableCap.CullFace);
            Gl.CullFace(CullFaceMode.Back);
          } else {
            Gl.Enable(EnableCap.CullFace);
            Gl.CullFace(CullFaceMode.Front);
          }

          //Gl.DrawElements(BeginMode.Triangles, _meshData.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
          Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);
        }
      }
      else if (renderStateIn == RenderState.RayCasting)
      {
        // set up the viewport and clear the previous depth and color buffers
        // make sure the shader program and texture are being used
        Gl.UseProgram(_volumeShader);
        Gl.BindBufferToShaderAttribute(_meshData, _volumeShader, "vertexPosition");
        Gl.BindBufferToShaderAttribute(_colorData, _volumeShader, "vertexColor");
        Gl.BindBufferToShaderAttribute(_normalsData, _volumeShader, "normals");

        Matrix4 inverseViewMatrix = SceneGraph<IGraphicObject>.ViewMatrix.Inverse();
        _volumeShader["inverse_view_matrix"].SetValue(inverseViewMatrix);
        Matrix4 inverseProjectionMatrix = SceneGraph<IGraphicObject>.ProjectionMatrix.Inverse();
        _volumeShader["inverse_projection_matrix"].SetValue(inverseProjectionMatrix);
        Matrix4 inverseModelMatrix = Matrix4.Identity.Inverse();
        _volumeShader["inverse_model_matrix"].SetValue(inverseModelMatrix);

        Gl.ActiveTexture(TextureUnit.Texture0);
        Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._entryExitRenderTarget[0]);
        int textureLocation0 = _volumeShader.GetUniformLocation("texture0");
        Gl.Uniform1i(textureLocation0, 0);

        Gl.ActiveTexture(TextureUnit.Texture1);
        Gl.BindTexture(TextureTarget.Texture2D, Program.Instance._entryExitRenderTarget[1]);
        int textureLocation1 = _volumeShader.GetUniformLocation("texture1");
        Gl.Uniform1i(textureLocation1, 1);

        Gl.ActiveTexture(TextureUnit.Texture2);
        Gl.BindTexture(TextureTarget.Texture3D, Program.Instance._volumeTexture[0]);
        int textureLocation2 = _volumeShader.GetUniformLocation("texture2");
        Gl.Uniform1i(textureLocation2, 2);


        Gl.BindBuffer(_meshData);

        Program demoProgram = Program.Instance;
        if (demoProgram != null) {
          Gl.Disable(EnableCap.CullFace);
          Gl.Disable(EnableCap.DepthTest);
          Gl.Enable(EnableCap.Blend);
          Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
          Vector3 newLocation = demoProgram.cameraLocation;
          _volumeShader["projection_matrix"].SetValue(SceneGraph<IGraphicObject>.ProjectionMatrix);
          _volumeShader["view_matrix"].SetValue(SceneGraph<IGraphicObject>.ViewMatrix);
          _volumeShader["model_matrix"].SetValue(Transform);

          if (frontFace) {
            Gl.Enable(EnableCap.CullFace);
            Gl.CullFace(CullFaceMode.Back);
          } else {
            Gl.Enable(EnableCap.CullFace);
            Gl.CullFace(CullFaceMode.Front);
          }

          //Gl.DrawElements(BeginMode.Triangles, _meshData.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
          Gl.DrawArrays(BeginMode.Triangles, 0, _meshData.Count);
        }
      }
    }

    public override void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {
      STLReader stlReader = new STLReader("..\\..\\STLModel\\3D_model_of_a_Cube.stl");
      

      TriangleMesh[] meshArray = stlReader.ReadFile();

      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();
      List<Vector2> generatedUVParam = new List<Vector2>();
      List<int> generatedIndices = new List<int>();


      Random random = new Random();
      generatedVertexData = new List<Vector3>();
      generatedColorData = new List<Vector3>();

      List<Vector3> colorList = new List<Vector3>();

      double intensity = 0.2;
      colorList.Add(new Vector3(intensity, 0.0, 0.0));
      colorList.Add(new Vector3(intensity, 0.0, 0.0));

      colorList.Add(new Vector3(0.0, intensity, 0.0));
      colorList.Add(new Vector3(0.0, intensity, 0.0));

      colorList.Add(new Vector3(0.0, 0.0, intensity));
      colorList.Add(new Vector3(0.0, 0.0, intensity));

      colorList.Add(new Vector3(intensity, intensity, 0.0));
      colorList.Add(new Vector3(intensity, intensity, 0.0));

      colorList.Add(new Vector3(0.0, intensity, intensity));
      colorList.Add(new Vector3(0.0, intensity, intensity));

      colorList.Add(new Vector3(intensity, 0.0, intensity));
      colorList.Add(new Vector3(intensity, 0.0, intensity));


      for (int i = 0; i < meshArray.Length; i++) {
        Vector_3 vertex1 = meshArray[i].vert1;
        Vector_3 vertex2 = meshArray[i].vert2;
        Vector_3 vertex3 = meshArray[i].vert3;

        int scaleFactor = 30;
        generatedVertexData.Add(new Vector3(vertex1.x / scaleFactor, vertex1.y / scaleFactor, vertex1.z / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex2.x / scaleFactor, vertex2.y / scaleFactor, vertex2.z / scaleFactor));
        generatedVertexData.Add(new Vector3(vertex3.x / scaleFactor, vertex3.y / scaleFactor, vertex3.z / scaleFactor));

        Vector_3 normal1 = meshArray[i].normal1;
        Vector_3 normal2 = meshArray[i].normal2;
        Vector_3 normal3 = meshArray[i].normal3;

        generatedNormalsData.Add(new Vector3(normal1.x, normal1.y, normal1.z));
        generatedNormalsData.Add(new Vector3(normal2.x, normal2.y, normal2.z));
        generatedNormalsData.Add(new Vector3(normal3.x, normal3.y, normal3.z));

        //generatedColorData.Add(colorList[i]);
        //generatedColorData.Add(colorList[i]);
        //generatedColorData.Add(colorList[i]);


        //generatedColorData.Add(new Vector3(random.NextDouble(), random.NextDouble(), random.NextDouble()));
        //generatedColorData.Add(new Vector3(random.NextDouble(), random.NextDouble(), random.NextDouble()));
        //generatedColorData.Add(new Vector3(random.NextDouble(), random.NextDouble(), random.NextDouble()));
      }

      _meshData = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsData = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorData = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _uvParamData = new VBO<Vector2>(generatedUVParam.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _meshIndices = new VBO<int>(generatedIndices.ToArray(), BufferTarget.ElementArrayBuffer);
      CreateShader();

      InitialiseChildData(200.0f, 200.0f, 40.0f, 2);
    }

    public void InitialiseChildData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel) {
      STLReader stlReader = new STLReader("..\\..\\STLModel\\3D_model_of_a_Cube.stl");

      TriangleMesh[] meshArray = stlReader.ReadFile();

      List<Vector3> generatedVertexData = new List<Vector3>();
      List<Vector3> generatedColorData = new List<Vector3>();
      List<Vector3> generatedNormalsData = new List<Vector3>();
      List<Vector2> generatedUVParam = new List<Vector2>();
      List<int> generatedIndices = new List<int>();

      generatedVertexData = new List<Vector3>();
      generatedColorData = new List<Vector3>();

      List<Vector3> colorList = new List<Vector3>();

      double intensity = 0.2;
      colorList.Add(new Vector3(intensity, 0.0, 0.0));
      colorList.Add(new Vector3(intensity, 0.0, 0.0));

      colorList.Add(new Vector3(0.0, intensity, 0.0));
      colorList.Add(new Vector3(0.0, intensity, 0.0));

      colorList.Add(new Vector3(0.0, 0.0, intensity));
      colorList.Add(new Vector3(0.0, 0.0, intensity));

      colorList.Add(new Vector3(intensity, intensity, 0.0));
      colorList.Add(new Vector3(intensity, intensity, 0.0));

      colorList.Add(new Vector3(0.0, intensity, intensity));
      colorList.Add(new Vector3(0.0, intensity, intensity));

      colorList.Add(new Vector3(intensity, 0.0, intensity));
      colorList.Add(new Vector3(intensity, 0.0, intensity));


      for (int i = 0; i < meshArray.Length; i++) {
        Vector_3 vertex1 = meshArray[i].vert1;
        Vector_3 vertex2 = meshArray[i].vert2;
        Vector_3 vertex3 = meshArray[i].vert3;

        generatedVertexData.Add(new Vector3((vertex1.x + 160)/ scaleFactorX, (vertex1.y +100)/ scaleFactorY, (vertex1.z +10)/ scaleFactorZ));
        generatedVertexData.Add(new Vector3((vertex2.x + 160)/ scaleFactorX, (vertex2.y +100)/ scaleFactorY, (vertex2.z +10)/ scaleFactorZ));
        generatedVertexData.Add(new Vector3((vertex3.x + 160)/ scaleFactorX, (vertex3.y +100)/ scaleFactorY, (vertex3.z +10)/ scaleFactorZ));

        Vector_3 normal1 = meshArray[i].normal1;
        Vector_3 normal2 = meshArray[i].normal2;
        Vector_3 normal3 = meshArray[i].normal3;

        generatedNormalsData.Add(new Vector3(normal1.x, normal1.y, normal1.z));
        generatedNormalsData.Add(new Vector3(normal2.x, normal2.y, normal2.z));
        generatedNormalsData.Add(new Vector3(normal3.x, normal3.y, normal3.z));

        //generatedColorData.Add(colorList[i]);
        //generatedColorData.Add(colorList[i]);
        //generatedColorData.Add(colorList[i]);
      }

      _meshDataChild = new VBO<Vector3>(generatedVertexData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _normalsDataChild = new VBO<Vector3>(generatedNormalsData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _colorDataChild = new VBO<Vector3>(generatedColorData.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _uvParamDataChild = new VBO<Vector2>(generatedUVParam.ToArray(), BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw);
      _meshIndicesChild = new VBO<int>(generatedIndices.ToArray(), BufferTarget.ElementArrayBuffer);
    }

    protected override void CreateTextures() {

    }

    private void ReInitShaderProgram() {
      if (_shader != null)
        _shader.Dispose();
      // create our shader program
      _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    private void ReInitVolumeShaderProgram() {
      if (_volumeShader != null)
        _volumeShader.Dispose();
      // create our shader program
      _volumeShader = new ShaderProgram(volumeVertexShaderString, volumeFragmentShaderString);
    }

    protected virtual void CreateShader() {
      // create our shader program
      if (!_updateShader)
        _shader = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    public override void Animate() {
      Matrix4 parentTransform = SceneGraph<IGraphicObject>.transformStack.Peek();
      Transform = Matrix4.Identity;
      Transform = Transform * parentTransform;
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\NewCubeVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\NewCubeFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }

    private string volumeVertexShaderString;
    private void ReadVolumeVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\NewVolumeVertexShader.txt";
      volumeVertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string volumeFragmentShaderString;
    private void ReadVolumetricFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\NewVolumeFragmentShader.txt";
      volumeFragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }


  }
}