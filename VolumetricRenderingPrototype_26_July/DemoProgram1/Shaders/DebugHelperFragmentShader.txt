#version 130

in vec3 color;
in vec2 uv;
in vec4 ShadowCoord;

uniform sampler2D texture0;
uniform sampler2D texture1;

out vec4 fragment;
//out float fragDepth;

float map(float value, float min1, float max1, float min2, float max2)
{
    return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

void main(void)
{
    //float lowLimit = 0.985;
    //float hightLimit = 1.0;
    //float myDepth = texture2D(texture0, uv).z;
    //float myDepthNew = map(myDepth, lowLimit, hightLimit, 0.0, 1.0);
    //fragment = vec4(myDepthNew, 0.0, 0.0, 1.0);

    fragment = texture2D(texture1, uv);
	fragment = fragment * 2;
	fragment = fragment- vec4(1.0, 1.0, 1.0, 1.0);
	if(fragment.z < 0.35)
		fragment = vec4(0.2, 0.0, 0.0, 1.0);
	else
		fragment = vec4(0.0, 0.2, 0.0, 1.0);
		
}