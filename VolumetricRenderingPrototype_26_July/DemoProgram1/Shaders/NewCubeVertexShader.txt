#version 130

in vec3 vertexPosition;
in vec3 vertexColor;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;

out vec3 color;
out vec4 position;

void main(void)
{
    color = vertexColor;
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1);
	position = gl_Position;
}