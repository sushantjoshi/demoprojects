#version 130

in vec3 color;
in vec2 uv;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler3D texture2;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;

out vec4 fragment;

void main(void)
{
	vec4 start = texture2D(texture1, uv);
	start = start * 2;
	start = start- vec4(1.0, 1.0, 1.0, 1.0);

	vec4 end = texture2D(texture0, uv);
	end = end * 2;
	end = end- vec4(1.0, 1.0, 1.0, 1.0);

	vec4 distance = start - end;
	vec3 opacityVector = vec3(distance.x, distance.y, distance.z);
	float opacity = length(opacityVector);

	if(opacity > 0.00000001)
		fragment = vec4(0.5, 0.5, 0.0, opacity);		
	
	start = model_matrix * view_matrix * projection_matrix * start;
	end = model_matrix * view_matrix * projection_matrix * end;

	vec3 _start = vec3(start.x, start.y, start.z);
	vec3 _end = vec3(end.x, end.y, end.z);
	
	int numOfSteps = 1000;
	float stepDelta = 0.002;
	vec4 accumulatedColor = vec4(0,0,0,0);
	vec4 currentColor = vec4(0,0,0,0);	
	if(opacity > 0.00000001)
	{
		_start = clamp(_start, vec3(0,0,0), vec3(1, 1, 1));
		_end = clamp(_end, vec3(0,0,0), vec3(1, 1, 1));
		vec3 currentLocation = _start;
		currentColor = texture3D(texture2, currentLocation);

		float distanceToTravel = length(_start - _end);
		vec3 rayDirection0 = normalize(_end- _start);
		float distanceTravelled = 0;

		for(int count = 0; count < numOfSteps; count++)
		{
			if(distanceTravelled < distanceToTravel && accumulatedColor.a < 0.95f)
			{
			currentColor = texture3D(texture2, currentLocation);
			currentColor.rgb *= currentColor.a;
			accumulatedColor = (1.0f - accumulatedColor.a)*currentColor + accumulatedColor;  
			currentLocation = currentLocation + stepDelta*rayDirection0;
			distanceTravelled = length(currentLocation - _start);
			}
		}
		fragment = accumulatedColor;
/*		fragment = vec4(rayDirection0, 1.0);
		fragment = vec4(0.5, 0.5, 0.0, 1.0);*/		
//		fragment = vec4(currentColor.r, currentColor.g, currentColor.b, opacity);
	}
//	fragment = vec4(0.5, 0.5, 0.0, opacity);		
}