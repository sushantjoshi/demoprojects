#version 130

out vec4 fragment;
in vec3 color;

void main(void)
{	
	fragment = vec4(color.xyz, 1.0);
	
	//fragment = vec4(0.2, 0.0, 0.0, 1.0);

	//gl_FragColor = vec4(color.xyz, 1.0);
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	//gl_FragColor = vec4(gl_Position.x, gl_Position.y, gl_Position.z, 1.0);
}