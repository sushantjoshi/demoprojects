﻿/**
  ******************************************************************************
  * @file    TriangleMesh.cs
  * @author  Ali Batuhan KINDAN
  * @version V1.0.0
  * @date    03.07.2018
  * @brief   
  ******************************************************************************
  */

namespace DemoProgram1
{
    public class TriangleMesh
    {
        public Vector_3 normal1;
        public Vector_3 normal2;
        public Vector_3 normal3;
        public Vector_3 vert1;
        public Vector_3 vert2;
        public Vector_3 vert3;

        /**
        * @brief  Class instance constructor
        * @param  none
        * @retval none
        */
        public TriangleMesh()
        {
          normal1 = new Vector_3();
          normal2 = new Vector_3();
          normal3 = new Vector_3();
          vert1 = new Vector_3();
          vert2 = new Vector_3();
          vert3 = new Vector_3();
        }
    }

}
