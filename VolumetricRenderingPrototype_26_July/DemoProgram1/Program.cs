﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.CompilerServices;
using Tao.FreeGlut;

namespace DemoProgram1 {
  class Program {
    public Stack<Matrix4> _transformStack = new Stack<Matrix4>();
    public static int width = 800, height = 600;

    private static arcball arcBall;

    private Matrix4f LastTransformation = new Matrix4f();
    private Matrix4f ThisTransformation = new Matrix4f();
    static SceneGraph<IGraphicObject> _sceneGraph;

    public uint[] _entryExitFrameBuffer = new uint[1];
    public uint[] _entryExitRenderTarget = new uint[2];
    public uint[] _volumeTexture = new uint[1];

    static public Program Instance {
      get;
      set;
    }

    int SIZE = 128;
    private float[] _data;
    //private float myAlpha1 = 1.0f;
    //private float myAlpha2 = 0.01f;
    //private float myAlpha3 = 0.0019611f;

    //private float myAlpha0 = 0.0f;
    //private float myAlpha1 = 0.05f;
    //private float myAlpha2 = 0.014f;
    //private float myAlpha3 = 0.001960888f;

    private float myAlpha0 = 0.0f;
    private float myAlpha1 = 0.05f;
    private float myAlpha2 = 0.00199f;
    private float myAlpha3 = 0.00199f;

    private void InitBlank3DTexture() {
      _data = new float[SIZE * SIZE * SIZE*4];
      for (var k = 0; k < SIZE; ++k) {
        for (var j = 0; j < SIZE; ++j) {
          for (var i = 0; i < SIZE; ++i) {
            _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.0f;
            _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.8f;
            _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
            _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = 0.001f;
          }
        }
      }
    }
    private void Create3DTexture() {
      Random random = new Random();
      _data = new float[SIZE * SIZE * SIZE * 4];
      for (var k = 0; k < SIZE; ++k) {
        for (var j = 0; j < SIZE; ++j) {
          for (var i = 0; i < SIZE; ++i) {
            //if (i == j) {
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.7f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha2;
            //}  else {
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.5f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha3;
            //}
            //if (i > 128 ) {
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.5f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha;
            //} else 
            //{
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.5f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
            //  _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha;
            //}
            if (i > 32 && i < 46 && j > 40 && j < 86) {
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.7f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha1;
            } else if (i > 80 && i < 90 && j > 40 && j < 86) {
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.7f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha1;
            } else if (i < 5 || i > 123 || j < 5 || j > 123 || k < 5 || k > 123) {
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.5f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha2;
            }
            else
            {
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 0] = 0.5f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 1] = 0.5f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 2] = 0.0f;
              _data[(i + (j * SIZE) + (k * SIZE * SIZE)) * 4 + 3] = myAlpha3;
            }
          }
        }
      }
    }

    private void InitialiseProgram() {

      _transformStack.Push(Matrix4.Identity);
      LastTransformation.SetIdentity(); // Reset Rotation
      ThisTransformation.SetIdentity(); // Reset Rotation

      // create an OpenGL window
      Glut.glutInit();
      Glut.glutSetOption(Glut.GLUT_MULTISAMPLE, 8);
      Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH | Glut.GLUT_MULTISAMPLE);
      //Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
      Glut.glutInitWindowSize(width, height);
      Glut.glutCreateWindow("testing");

      // provide the Glut callbacks that are necessary for running this tutorial
      Glut.glutIdleFunc(OnRenderFrame);
      Glut.glutCloseFunc(OnClose);
      Glut.glutMouseFunc(OnMouse);
      Glut.glutMotionFunc(OnMove);

      Gl.Enable(EnableCap.DepthTest);
      Gl.Enable(EnableCap.Multisample);
      Gl.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);
      var test = Glut.glutGet(Glut.GLUT_MULTISAMPLE);

      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

      //frame buffers render targets for reflection map
      Gl.GenFramebuffers(1, _entryExitFrameBuffer);
      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, _entryExitFrameBuffer[0]);

      Gl.GenTextures(2, _entryExitRenderTarget);
      for (int renderTextures = 0; renderTextures < 2; renderTextures++) {
        Gl.BindTexture(TextureTarget.Texture2D, _entryExitRenderTarget[renderTextures]);
        Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb32f, width, height, 0, PixelFormat.Rgb, PixelType.Float, System.IntPtr.Zero);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Linear);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
        Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
        Gl.BindTexture(TextureTarget.Texture2D, 0);
      }

      InitBlank3DTexture();
      Create3DTexture();
      int size = Marshal.SizeOf(_data[0]) * _data.Length;
      IntPtr pnt = Marshal.AllocHGlobal(size);
      try {
        // Copy the array to unmanaged memory.
        Marshal.Copy(_data, 0, pnt, _data.Length);
        Gl.GenTextures(1, _volumeTexture);
        Gl.BindTexture(TextureTarget.Texture3D, _volumeTexture[0]);
        Gl.TexImage3D(TextureTarget.Texture3D, 0, PixelInternalFormat.Rgba, SIZE, SIZE, SIZE, 0, PixelFormat.Rgba, PixelType.Float, pnt);
        Gl.TexParameteri(TextureTarget.Texture3D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);
        Gl.TexParameteri(TextureTarget.Texture3D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
        Gl.TexParameteri(TextureTarget.Texture3D, TextureParameterName.TextureWrapR, TextureParameter.ClampToEdge);
        Gl.TexParameteri(TextureTarget.Texture3D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
        Gl.TexParameteri(TextureTarget.Texture3D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
        Gl.BindTexture(TextureTarget.Texture3D, 0);
        //// Copy the unmanaged array back to another managed array.
        //float[] _testData = new float[_data.Length];
        //Marshal.Copy(pnt, _testData, 0, _data.Length);
      }
      catch {

      }

      Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

      arcBall = new arcball(width, height);
      arcBall.setBounds((float)width, (float)height); // Update mouse bounds for arcball

      Cube cube = new Cube();
      cube.InitialiseData(0.5f, 0.5f, 0.5f, 2);
      _sceneGraph = new SceneGraph<IGraphicObject>(cube);
      _renderTextureToScreenDebugObject = new RenderTextureToScreenDebugObject();
      _renderTextureToScreenDebugObject.InitialiseData();
      Glut.glutMainLoop();
    }

    static void Main(string[] args) {
      Instance = new Program();
      Instance.InitialiseProgram();
    }

    private static void OnClose() {

    }

    private enum mouseState {
      None,
      Pan,
      Zoom,
      Rotate
    }
    private static mouseState _mouseState = mouseState.None;
    private void OnMouse(int button, int state, int x, int y) {
      if (button == Glut.GLUT_LEFT_BUTTON) {
        _mouseState = mouseState.Rotate;
        System.Drawing.Point MousePt = new System.Drawing.Point(x, y);
        arcBall.click(MousePt); // Update Start Vector And Prepare For Dragging
        LastTransformation.set_Renamed(ThisTransformation); // Set Last Static Rotation To Last Dynamic One           
      } else if (button == Glut.GLUT_MIDDLE_BUTTON) {
        _mouseState = mouseState.Zoom;
        mouseZoomStart = new Vector2(x, y);
      }
    }

    private Vector3 cameraXAxis = new Vector3(1, 0, 0);
    public Vector3 cameraYAxis = new Vector3(0, 1, 0);
    public Vector3 cameraLocation = new Vector3(0, 0, -1);

    private Vector2 mouseZoomStart = new Vector2(0, 0);
    private float _cameraDistance = 1;
    private void OnMove(int x, int y) {
      if (_mouseState == mouseState.Rotate) {
        Quat4f ThisQuat = new Quat4f();
        System.Drawing.Point MousePt = new System.Drawing.Point(x, y);
        arcBall.drag(MousePt, ThisQuat); // Update End Vector And Get Rotation As Quaternion

        ThisTransformation.Pan = new Vector3f(0, 0, 0);
        ThisTransformation.Scale = 1.0f;
        ThisTransformation.Rotation = ThisQuat;
        ThisTransformation.MatrixMultiply(ThisTransformation, LastTransformation);

        float[] cameraRotation = new float[16];
        ThisTransformation.get_Renamed(cameraRotation);
        Matrix4 cameraRotationMatrix = new Matrix4(cameraRotation);
        cameraLocation = new Vector3(0, 0, _cameraDistance);
        cameraLocation = cameraRotationMatrix * cameraLocation;

        cameraYAxis = cameraRotationMatrix * new Vector3(0, 1, 0);
      } else if (_mouseState == mouseState.Zoom) {
        Vector2 mouseZoomEnd = new Vector2(x, y);
        float increment = (mouseZoomStart - mouseZoomEnd).Y;

        _cameraDistance = Convert.ToSingle(_cameraDistance + increment * 0.0005);

        if (_cameraDistance > 15)
          _cameraDistance = 15;

        if (_cameraDistance < 3)
          _cameraDistance = 3;

        Quat4f ThisQuat = new Quat4f();
        System.Drawing.Point MousePt = new System.Drawing.Point(x, y);
        arcBall.drag(MousePt, ThisQuat); // Update End Vector And Get Rotation As Quaternion

        ThisTransformation.Pan = new Vector3f(0, 0, 0);
        ThisTransformation.Scale = 1.0f;
        ThisTransformation.Rotation = ThisQuat;
        ThisTransformation.MatrixMultiply(ThisTransformation, LastTransformation);

        float[] cameraRotation = new float[16];
        ThisTransformation.get_Renamed(cameraRotation);
        Matrix4 cameraRotationMatrix = new Matrix4(cameraRotation);
        cameraLocation = new Vector3(0, 0, _cameraDistance);
        cameraLocation = cameraRotationMatrix * cameraLocation;
        cameraYAxis = cameraRotationMatrix * new Vector3(0, 1, 0);
      }
    }

    private RenderTextureToScreenDebugObject _renderTextureToScreenDebugObject = null;

    private bool _debugTexture = true;
    private bool _debugFront = true;// = _entryExitRenderTarget[0 or 1];
    private bool _renderOriginal = false;
    private bool _renderVolume = true;
    private bool _dynamic = true;

    private void OnRenderFrame() {
      // set up the viewport and clear the previous depth and color buffers
      Gl.Viewport(0, 0, width, height);
      Gl.ClearColor(0.5f, 0.5f, 0.5f, 1.0f);
      Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

      if (_renderOriginal) {
        Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        Gl.Viewport(0, 0, width, height);
        Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        _sceneGraph.DepthTraversal(false, false);
      } else if (_renderVolume) {
        //draw the front and back panel of the volume enclosed
        _sceneGraph.DepthTraversal(true, false);
        _sceneGraph.DepthTraversal(false, true);
        //_renderTextureToScreenDebugObject.VolumeTrace(_entryExitRenderTarget[0], _entryExitRenderTarget[1]);
      } else {
        if (_debugTexture) {
          if (_dynamic)
            _sceneGraph.DepthTraversal(true, false);
          if (_debugFront) {
            _renderTextureToScreenDebugObject.Render(_entryExitRenderTarget[0]);

          } else {
            _renderTextureToScreenDebugObject.Render(_entryExitRenderTarget[1]);
          }
        }
      } ;
      Glut.glutSwapBuffers();
    }
  }
}
