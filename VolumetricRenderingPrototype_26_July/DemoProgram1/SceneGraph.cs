﻿using OpenGL;
using System.Collections.Generic;


namespace DemoProgram1
{
    public class KTree<T>
    {
        public KTree(T nodeData)
        {
            _nodeData = nodeData;
        }

        public KTree<T> AddChild(T nodeData)
        {
            KTree<T> newNode = new KTree<T>(nodeData);
            _children.Add(newNode);
            return newNode;
        }

        public void DepthTraversal(bool renderingEntryExit, bool renderingVolume)
        {
            //do something with the _nodeData
            IGraphicObject graphicObject = _nodeData as IGraphicObject;
            if (graphicObject != null)
            {
                graphicObject.Animate();

                SceneGraph<IGraphicObject>.transformStack.Push(graphicObject.Transform);

                Gl.DepthMask(true);

                RenderState renderState = RenderState.Default;
                if (renderingEntryExit)
                {
                  graphicObject.Render(RenderState.Entry);
                  graphicObject.Render(RenderState.Exit);
                }
                else if (renderingVolume)
                {
                  graphicObject.Render( RenderState.RayCasting);
                }
                else
                {
                  graphicObject.Render(RenderState.Default);
                }

                //now traverse each child subtree
                foreach (KTree<T> subTree in _children)
                {
                    subTree.DepthTraversal(renderingEntryExit, renderingVolume);
                }

                SceneGraph<IGraphicObject>.transformStack.Pop();
            }
        }

        protected T _nodeData;
        protected List<KTree<T>> _children = new List<KTree<T>>();
    }

    public enum RenderState
    {
        Entry,
        Exit,
        Default,
        RayCasting
    }

    public class SceneGraph<T> : KTree<T>
    {
        public static Matrix4 ProjectionMatrix
        {
            get;
            private set;
        }

        public static Matrix4 ReflectionProjectionMatrix
        {
            get;
            private set;
        }

        public static Matrix4 ViewMatrix
        {
            get
            {
                Program demoProgram = Program.Instance;
                return Matrix4.LookAt(demoProgram.cameraLocation, Vector3.Zero, demoProgram.cameraYAxis);
            }
            set
            {

            }
        }

        public static Matrix4 ShadowMapCamera
        {
            get;
            set;
        }

        public static Matrix4 ReflectionMapCamera
        {
            get;
            set;
        }

        public static Matrix4 BiasMatrix
        {
            get;
            private set;
        }

        public SceneGraph(T nodeData)
            : base(nodeData)
        {
            transformStack.Push(Matrix4.Identity);
            //ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(0.55f, (float)1000 / 750, 0.01f, 30f);
            ProjectionMatrix = Matrix4.CreateOrthographic(3.6f, 2.5f, -1.8f, 2.8f);
            ReflectionProjectionMatrix = Matrix4.CreateOrthographic(1.2f, 0.4f, -2.05f, 10.0f);

            float[] biasMatrixParams = new float[] { 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f };
            BiasMatrix = new Matrix4(biasMatrixParams);//.Transpose();
            //BiasMatrix = Matrix4.Identity;
        }

        public static Stack<Matrix4> transformStack = new Stack<Matrix4>();
    }
}