﻿using OpenGL;
using System;
using System.Collections.Generic;
using System.IO;

namespace DemoProgram1 {
  class RenderTextureToScreenDebugObject {
    private VBO<Vector3> meshData;
    private VBO<Vector3> colorData;
    private VBO<Vector2> uvParamData;
    private VBO<int> meshIndices;
    private ShaderProgram _shader0;
    private ShaderProgram _shader1;

    private static bool _updateShader = true;

    public void InitialiseData() {
      meshData = new VBO<Vector3>(new Vector3[] { new Vector3(-1, -1, 0), new Vector3(-1, 1, 0), new Vector3(1, 1, 0), new Vector3(1, -1, 0), });
      colorData = new VBO<Vector3>(new Vector3[] { new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1), new Vector3(0.5f, 0.5f, 1) });
      uvParamData = new VBO<Vector2>(new Vector2[] { new Vector2(0.0f, 0.0f), new Vector2(0.0f, 1.0f), new Vector2(1.0f, 1.0f), new Vector2(1.0f, 0.0f) });
      meshIndices = new VBO<int>(new int[] { 0, 1, 2, 2, 3, 0 }, BufferTarget.ElementArrayBuffer);

      CreateTextures();
      CreateShader();
    }

    public void VolumeTrace(uint frontFaceTextureID, uint backFaceTextureID)
    {
      if (_updateShader)
      {
        try
        {
          ReadVolumeVertexShaderFromFile();
          ReadVolumeFragmentShaderFromFile();
          ReInitVolumeShaderProgram();
          _updateShader = false;
        }
        catch (Exception e)
        {
        }
      }

      Gl.UseProgram(_shader1);
        //Gl.BindTexture(_textureToDebug);

        Gl.BindBufferToShaderAttribute(meshData, _shader1, "vertexPosition");
        Gl.BindBufferToShaderAttribute(colorData, _shader1, "vertexColor");
        Gl.BindBufferToShaderAttribute(uvParamData, _shader1, "vertexUV");
        Gl.BindBuffer(meshIndices);

        Program demoprogram = Program.Instance;
        if (demoprogram != null) {
          Vector3 newLocation = demoprogram.cameraLocation;

          Vector3 tempCameraXAxis = new Vector3(1, 0, 0);
          Vector3 tempCameraYAxis = new Vector3(0, 1, 0);
          Vector3 tempCameraLocation = new Vector3(0, 0, 10);

          Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
          Gl.Viewport(0, 0, Program.width, Program.height);
          Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

        Matrix4 inverseViewMatrix = SceneGraph<IGraphicObject>.ViewMatrix.Inverse();
          _shader1["view_matrix"].SetValue(inverseViewMatrix);
          Matrix4 inverseProjectionMatrix = SceneGraph<IGraphicObject>.ProjectionMatrix.Inverse();
          _shader1["projection_matrix"].SetValue(inverseProjectionMatrix);
          Matrix4 inverseModelMatrix = Matrix4.Identity.Inverse();
          _shader1["model_matrix"].SetValue(inverseModelMatrix);

          Gl.ActiveTexture(TextureUnit.Texture0);
          Gl.BindTexture(TextureTarget.Texture2D, backFaceTextureID);
          int textureLocation0 = _shader1.GetUniformLocation("texture0");
          Gl.Uniform1i(textureLocation0, 0);

          Gl.ActiveTexture(TextureUnit.Texture1);
          Gl.BindTexture(TextureTarget.Texture2D, frontFaceTextureID);
          int textureLocation1 = _shader1.GetUniformLocation("texture1");
          Gl.Uniform1i(textureLocation1, 1);

          Gl.ActiveTexture(TextureUnit.Texture2);
          Gl.BindTexture(TextureTarget.Texture3D, Program.Instance._volumeTexture[0]);
          int textureLocation2 = _shader1.GetUniformLocation("texture2");
          Gl.Uniform1i(textureLocation2, 2);

          Gl.Disable(EnableCap.Blend);
          //Gl.Enable(EnableCap.Blend);
          Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
          Gl.DrawElements(BeginMode.Triangles, meshIndices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
          //Gl.Disable(EnableCap.Blend);
      }
    }

    public void Render(uint textureID) {

      if (_updateShader) {
        try {
          ReadVertexShaderFromFile();
          ReadFragmentShaderFromFile();
          ReInitShaderProgram();
          _updateShader = false;
        }
        catch (Exception e) {
        }
      }

      Gl.UseProgram(_shader0);
      //Gl.BindTexture(_textureToDebug);

      Gl.BindBufferToShaderAttribute(meshData, _shader0, "vertexPosition");
      Gl.BindBufferToShaderAttribute(colorData, _shader0, "vertexColor");
      Gl.BindBufferToShaderAttribute(uvParamData, _shader0, "vertexUV");
      Gl.BindBuffer(meshIndices);

      Program demoprogram = Program.Instance;
      if (demoprogram != null) {
        Vector3 newLocation = demoprogram.cameraLocation;

        Vector3 tempCameraXAxis = new Vector3(1, 0, 0);
        Vector3 tempCameraYAxis = new Vector3(0, 1, 0);
        Vector3 tempCameraLocation = new Vector3(0, 0, 10);

        Matrix4 inverseViewMatrix = Matrix4.LookAt(tempCameraLocation, Vector3.Zero, tempCameraYAxis).Inverse();
        _shader0["view_matrix"].SetValue(inverseViewMatrix);

        //_shader0["view_matrix"].SetValue(Matrix4.LookAt(newLocation, Vector3.Zero, demoprogram.cameraYAxis));
        //_shader0["view_matrix"].SetValue(Matrix4.Identity);
        _shader0["biasMatrix"].SetValue(SceneGraph<IGraphicObject>.BiasMatrix);
        Matrix4 inverseProjectionMatrix = SceneGraph<IGraphicObject>.ProjectionMatrix.Inverse();
        _shader0["projection_matrix"].SetValue(inverseProjectionMatrix);
        Matrix4 inverseModelMatrix = Matrix4.Identity.Inverse();
        _shader0["model_matrix"].SetValue(inverseModelMatrix);

        //Gl.ActiveTexture(TextureUnit.Texture0);
        //Gl.BindTexture(TextureTarget.Texture2D, textureID);
        //int textureLocation0 = _shader0.GetUniformLocation("texture0");
        //Gl.Uniform1i(textureLocation0, 0);

        Gl.ActiveTexture(TextureUnit.Texture1);
        //Gl.BindTexture(TextureTarget.Texture2D, _textureToDebug.TextureID);
        Gl.BindTexture(TextureTarget.Texture2D, textureID);
        int textureLocation1 = _shader0.GetUniformLocation("texture1");
        Gl.Uniform1i(textureLocation1, 1);

        Gl.DrawElements(BeginMode.Triangles, meshIndices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
      }
    }

    private void ReInitVolumeShaderProgram()
    {
      if (_shader1 != null)
        _shader1.Dispose();
      // create our shader program
      _shader1 = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }

    private void ReInitShaderProgram() {
      if (_shader0 != null)
        _shader0.Dispose();
      // create our shader program
      _shader0 = new ShaderProgram(vertexShaderString, fragmentShaderString);
    }
    protected virtual void CreateShader() {
      // create our shader program
      //if (!_updateShader)
      //  _shader0 = new ShaderProgram(vertexShaderString, fragmentShaderString);
      //_shader0 = new ShaderProgram(RenderToTextureVertexSourceString, RenderToTextureFragmentSourceString);
    }

    private static Texture _textureToDebug;
    public void CreateTextures() {
      // load a debug texture
      if (_textureToDebug == null) {
        _textureToDebug = new Texture("C:\\Users\\320057092\\Downloads\\DemoProject1\\DemoProgram1\\logo.jpg");
      }
    }

    private string vertexShaderString;
    private void ReadVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\DebugHelperVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string fragmentShaderString;
    private void ReadFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\DebugHelperFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }

    private string volumeVertexShaderString;
    private void ReadVolumeVertexShaderFromFile() {
      string vertexShaderFile = "..\\..\\Shaders\\VolumeVertexShader.txt";
      vertexShaderString = File.ReadAllText(vertexShaderFile);
    }
    private string volumeFragmentShaderString;
    private void ReadVolumeFragmentShaderFromFile() {
      string fragmentShaderFile = "..\\..\\Shaders\\VolumeFragmentShader.txt";
      fragmentShaderString = File.ReadAllText(fragmentShaderFile);
    }
  }
}
