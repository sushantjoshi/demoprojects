﻿using OpenGL;
using System;
using System.Collections.Generic;


namespace DemoProgram1
{
    interface IGraphicObject
    {
        void Render(RenderState renderStateIn);
        void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel);
        void Animate();

        Matrix4 Transform
        {
            get;
            set;
        }
    }

    abstract class GraphicObjectBase: IGraphicObject
    {
        public abstract void Render(RenderState renderStateIn);
        public abstract void InitialiseData(float scaleFactorX, float scaleFactorY, float scaleFactorZ, int detailLevel);
        public abstract void Animate();

        public Matrix4 Transform
        {
            get;
            set;
        }

        protected abstract void CreateTextures();

        protected VBO<Vector3> _meshData;
        protected VBO<Vector3> _colorData;
        protected VBO<Vector3> _normalsData;
        protected VBO<Vector2> _uvParamData;
        protected VBO<int> _meshIndices;
        protected ShaderProgram _shader;
    }
}