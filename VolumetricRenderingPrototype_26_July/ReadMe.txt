Demo OpenGL project
A demo OpenGL project that implements a basic volumetric rendering algorithm. The project is written in C# using OpenTK wrapper for OpenGL. 
The demo shows a green/yellow cube having translucent properties, with two zones of different color (red and blue bars) which are relatively opaque. Final color of each pixel depends upon the viewing direction.
